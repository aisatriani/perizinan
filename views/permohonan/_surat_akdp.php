<?php

use app\components\DateIndo;

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>


<table width="100%">
    <TR>
        <TD ALIGN="CENTER" WIDTH="20%"><IMG SRC="<?=

            \yii\helpers\Url::to('@web/img/prov-gorontalo.jpg') ?>"
                                            WIDTH="13%"></TD>
        <TD ALIGN="CENTER" WIDTH="60%"><span style="font-size: 14pt">PEMERINTAH PROVINSI GORONTALO</span><BR>
            <span style="font-size: 16pt"><b>BADAN PENANAMAN MODAL DAN PTSP</b></span><BR>
            <span style="font-size: 8pt"><I>Jln. Tengah Desa Toto Selatan Kec. Kabila Kab. Bone Bolango Telp (0435) 8591278 Fax.
                    (0435) 8591277</span>
        </TD>
    </TR>
</table>
<hr style="border-top: 3px double #8c8b8b;">
<BR>

<p style="font-size: 10pt; width: 90%;" align="center"><b>KEPUTUSAN KEPALA BADAN PENANAMAN
    MODAL DAN PELAYANAN TERPADU SATU PINTU PROVINSI GORONTALO  <br>
    NOMOR : <?= $model->pemohonDetail->no_terbit ?> <br>
        TENTANG <br>
        SURAT <?= strtoupper($model->idJenisPerizinan->nama_izin) ?> (<?= strtoupper($model->idJenisPerizinan->singkatan) ?>)
    </b></p>

<br>



<div class="row">
    <div class="col-xs-2">
        Membaca
    </div>
    <div class="col-xs-9">
        Surat permohonan dari <?= $model->idPerusahaan->nama_perusahaan ?> tanggal <?= DateIndo::createDate(1) ?>
    </div>
</div>
	<div class="row">
		<div class="col-xs-2">
            Menimbang
		</div>
        <div class="col-xs-9">
            <ol type="a" style="padding-left: 20px;">
                <li>Maksud permohonan untuk merealisasikan Izin Trayek :
                    TERMINAL <?= $terminal_asal ?> - TERMINAL <?= $terminal_tujuan ?> , PP
                    dengan kendaraan jenis Minibus / Mikrolet daya angkut 10 SEAT + KG BARANG

                </li>
                <li>Masih dimungkinkan pemberian Izin Trayek Angkutan Antar Kota Dalam Provnsi.</li>
            </ol>

        </div>
	</div>
<div class="row">
    <div class="col-xs-2">
        Mengingat
    </div>
    <div class="col-xs-9">
        <ol type="1" style="padding-left: 20px;">
            <li>Undang-Undang Nomor 38 Tahun 2000 tentang Pembentukan Provinsi Gorontalo.</li>
            <li>Undang-Undang Nomor 22 tahun 2009 tentang Lalu Lintas dan Angkutan Jalan.</li>
            <li>Peraturan Pemerintah Nomor 74 tahun 2014 tentang Angkutan Jalan.</li>
            <li>Keputusan Menteri Perhubungan Nomor KM 35 tahun 2003 tentang Penyelenggaraan Angkutan Orang di Jalan Dengan Kendaraan Umum.</li>
            <li>Peraturan Gubernur Gorontalo Nomor 42 Tahun 2015 Tentang Pendelegasian kewenangan untuk Menandatangani Perizinan dan non Perizinan kepada Badan Penanaman Modal dan Pelayanan Terpadu Satu Pintu (BPM-PTSP) Provinsi Gorontalo.</li>
        </ol>

    </div>
</div>

<p align="center"><b> MEMUTUSKAN </b></p>
<p>Menetapkan :</p>
<div class="row">
    <div class="col-xs-2">
        PERTAMA
    </div>
    <div class="col-xs-9">
        Memberikan Izin Trayek Angkutan Antar Kota Dalam Provinsi untuk melayani penumpang umum pada trayek : TERMINAL			- TERMINAL
        <table style="margin-top: 10px; margin-bottom: 10px;">
            <tr>
                <td>Tanda Nomor Kendaraan</td>
                <td>: <?= $plat_no ?></td>
            </tr>
            <tr>
                <td>Nama Pemilik</td>
                <td>: <?= $nama_pemilik ?></td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>: <?= $alamat ?></td>
            </tr>
            <tr>
                <td>Tahun Pembuatan</td>
                <td>: <?= $tahun_pembuatan ?></td>
            </tr>
            <tr>
                <td>No Uji Berkala</td>
                <td>: <?= $no_uji ?></td>
            </tr>
        </table>


    </div>
</div>
<div class="row">
    <div class="col-xs-2">
        KEDUA
    </div>
    <div class="col-xs-9">
        Masa berlaku Izin terhitung mulai tanggal : <?= $tahun_terbit ?> S/D <?= $tahun_berakhir ?>.
    </div>
</div>

<div class="row">
    <div class="col-xs-2">
        KETIGA
    </div>
    <div class="col-xs-9">
        Kewajiban pemegang Izin Trayek tercantum dibalik Surat Keputusan ini.
    </div>
</div>

<div class="row">
    <div class="col-xs-2">
        KEEMPAT
    </div>
    <div class="col-xs-9">
        Lembaran Asli Surat Keputusan ini diberikan kepada yang bersangkutan.
    </div>
</div>

<div class="row">
    <div class="col-xs-2">
        KELIMA
    </div>
    <div class="col-xs-9">
        Keputusan ini berlaku sejak tanggal ditetapkan dengan ketentuan jika dikemudian hari terdapat kekeliruan dalam penetapannya akan diadakan perbaikan seagaimana mestinya.
    </div>
</div>


<p align="right" style="margin-top: 50px;"> Ditetapkan di Gorontalo <br>
    Pada tanggal, <?= DateIndo::createDate(DateIndo::FORMAT_BULAN_TEXT) ?></p>
<p align="right" style=""> </p>


	<table align="right" width="30%">
        <tr style="border-spacing: 10px">
            <td align="center">a.n GUBERNUR GORONTALO</td>
        </tr>
        <tr>
            <td align="center">KEPALA BPMPTSP PROVINSI GORONTALO</td>
        </tr>
        <tr>
            <td><br><br><br><br><br></td>
        </tr>
        <tr>
            <td align="center"><u>Ir. HENRY F. DJUNA, MM</u></td>
        </tr>
        <tr>
            <td align="">NIP. 19570909 199203 1 001</td>
        </tr>

    </table>

<p style="margin-top: 20px; font-weight: bold;" class="" align="center">KEWAJIBAN PEMEGANG IZIN TRAYEK</p>
<p>Perusahaan Angkutan yang telah memegang Izin Trayek diwajibkan untuk :</p>
<ol type="1">
    <li>Mengoperasikan kendaraan sesuai dengan jenis pelayanan berdasarkan Izin Trayek dimiliki.</li>
    <li>Mengoperasikan kendaraan bermotor yang memenuhi persyaratan teknis dan laik jalan.</li>
    <li>Mempekerjakan awak Kendaraan yang memenuhi persyaratan teknis sesusai dengan ketentuan perundang-undangan yang berlaku merupakan pengemudi serta pegawai tetap perusahaan.</li>
    <li>Memenuhi waktu kerja dan waktu istrahat bagi si pengemudi.</li>
    <li>Memiliki tanda bukti pembayaran iuran wajib asuransi pertanggungan kecelakaan penumpang sebagaimana dimaksud dalam Undang-Undang Nomor 33 Tahun 1964 beserta Peraturan Pelaksanaannya.</li>
    <li>Melaporkan apabila terjadi perubahan kepemilikan perusahaan atau domisili perusahaan</li>
    <li>Meminta pengesahan dari pejabat pemberi Izin Trayek, apabila akan mengalihkan Izin Trayek.</li>
    <li>Melaporkan setiap bulan kegiatan operasional angkutan.</li>
    <li>Melaporkan secara tertulis kepada Pejabat pemberi Izin Trayek, apabila terjadi perubahan alamat selambat-lambatnya 14 (empat belas) hari setelah terjadi perubahan.</li>
    <li>Melayani trayek sesuai izin yang diberikan dengan cara :
        <ol type="a" style="padding-left: 20px;">
            <li>Mengoperasikan kendaraan secara tepat waktu sejak saat pemberangkatan, persinggahan dan sampai tujuan.</li>
            <li>Memelihara kebersihan dan kenyamanan kendaraan yang dioperasikan.</li>
            <li>Memberikan pelayanan sebaik-baiknya kepada penmpang.</li>
            <li>Membawa Kartu Pengawasan Dalam Operasinya.</li>
        </ol>
    </li>
    <li></li>
    <li></li>
</ol>



</body>
</html>