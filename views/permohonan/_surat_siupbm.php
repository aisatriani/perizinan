<?php

use app\components\DateIndo;

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>

</head>
<body>


<table width="100%">
    <TR>
        <TD ALIGN="CENTER" WIDTH="20%"><IMG SRC="<?=

            \yii\helpers\Url::to('@web/img/prov-gorontalo.jpg') ?>"
                                            WIDTH="13%"></TD>
        <TD ALIGN="CENTER" WIDTH="60%"><span style="font-size: 14pt">PEMERINTAH PROVINSI GORONTALO</span><BR>
            <span style="font-size: 16pt"><b>BADAN PENANAMAN MODAL DAN PTSP</b></span><BR>
            <span style="font-size: 8pt"><I>Jln. Tengah Desa Toto Selatan Kec. Kabila Kab. Bone Bolango Telp (0435) 8591278 Fax.
                    (0435) 8591277</span>
        </TD>
    </TR>
</table>
<hr style="border-top: 3px double #8c8b8b;">
<BR>

<p style="font-size: 10pt; width: 90%;" align="center"><b>KEPUTUSAN KEPALA BADAN PENANAMAN
    MODAL DAN PELAYANAN TERPADU SATU PINTU PROVINSI GORONTALO  <br>
    NOMOR : <?= $model->pemohonDetail->no_terbit ?> <br>
        TENTANG <br>
        SURAT <?= strtoupper($model->idJenisPerizinan->nama_izin) ?> (<?= strtoupper($model->idJenisPerizinan->singkatan) ?>)
    </b></p>

<br>



<div class="row">
    <div class="col-xs-2">
        Membaca
    </div>
    <div class="col-xs-9">
        Surat permohonan dari <?= $model->idPerusahaan->nama_perusahaan ?> Tanggal <?= DateIndo::createDate(1) ?>
    </div>
</div>
	<div class="row">
		<div class="col-xs-2">
            Menimbang
		</div>
        <div class="col-xs-9">
            Maksud permohonan untuk merealisasikan Surat Izin Usaha Perusahaan Bongkar Muat (SIUPBM)
        </div>
	</div>
<div class="row">
    <div class="col-xs-2">
        Mengingat
    </div>
    <div class="col-xs-9">
        <ol type="1" style="padding-left: 20px;">
            <li>Undang-Undang Nomor 38 Tahun 2000 tentang Pembentukan Provinsi Gorontalo.</li>
            <li>Undang-Undang Nomor 17 Tahun 2008 tentang Pelayaran.</li>
            <li>Peraturan Pemerintah Nomor 22 Tahun 2011 tentang kepelabuhan.</li>
            <li>Peraturan Pemerintah Nomor 22 Tahun 2011 tentang Perubahan atas Peraturan Pemerintah Nomor 20 Tahun 2010 tentang Angkutan di Perairan</li>
            <li>Peraturan Menteri Perhubungan Republik Indonesia Nomor PM 93 Tahun 2014 tentang Penyelenggaraan dan Pengusahaan Bonkar Muat Barang Dari dan Ke Kapal.</li>
            <li>Peraturan Gubernur Gorontalo Nomor 80 Tahun 2014 Tentang Pendelegasian kewenangan untuk Menandatangani Periznan dan Non Perizinan kepada Badan Penanaman Modal dan Pelayanan Terpadu Satu Pintu (BPM-PTSP) Provinsi Gorontalo.</li>
        </ol>

    </div>
</div>

<p align="center"><b> MEMUTUSKAN </b></p>
<p>Menetapkan :</p>
<div class="row">
    <div class="col-xs-2">
        PERTAMA
    </div>
    <div class="col-xs-9">
        Memberikan Surat Izin Usaha Perusahaan Bongkar Muat (SIUPBM) Kepada :
        <table style="margin-top: 10px; margin-bottom: 10px;">
            <tr>
                <td>Nama Perusahaan</td>
                <td>: <?= $model->idPerusahaan->nama_perusahaan ?></td>
            </tr>
            <tr>
                <td>Alamat Perusahaan</td>
                <td>: <?= $model->idPerusahaan->jalan ?>, <?= $model->idPerusahaan->kelurahan0->nama_kel ?> </td>
            </tr>
            <tr>
                <td>NPWP Perusahaan</td>
                <td>: <?= $model->idPerusahaan->npwp ?></td>
            </tr>

        </table>


    </div>
</div>
<div class="row">
    <div class="col-xs-2">
        KEDUA
    </div>
    <div class="col-xs-9">
        Surat Izin Usaha Perusahaan Bongkar Muat (SIUPBM) ini dapat dicabut apabila pemegang SIUPBM ini tidak mematuhi kewajiban dalam SIUPBM dan atau melakukan tindak pidana yang bersangkutan dengan kegiatan usahanya.
    </div>
</div>

<div class="row">
    <div class="col-xs-2">
        KETIGA
    </div>
    <div class="col-xs-9">
        Surat Izin Usaha Perusahaan Bongkar Muat (SIUPBM) ini berlaku selama Perusahaan yang bersangkutan masih menjalankan usahanya.
    </div>
</div>

<div class="row">
    <div class="col-xs-2">
        KEEMPAT
    </div>
    <div class="col-xs-9">
        Lembaran Asli Surat Keputusan ini diberikan kepada perusahaan yang bersangkutan.
    </div>
</div>

<div class="row">
    <div class="col-xs-2">
        KELIMA
    </div>
    <div class="col-xs-9">
        Keputusan ini berlaku sejak tanggal ditetapkan dengan ketentuan jika dikemudian hari terdapat kekeliruan dalam penetapannya akan diadakan perbaikan sebagaimana mestinya.
    </div>
</div>


<p align="right" style="margin-top: 50px;"> Ditetapkan di Gorontalo <br>
    Pada tanggal, <?= DateIndo::createDate(DateIndo::FORMAT_BULAN_TEXT) ?></p>
<p align="right" style=""> </p>


	<table align="right" width="30%">
        <tr style="border-spacing: 10px">
            <td align="center">a.n GUBERNUR GORONTALO</td>
        </tr>
        <tr>
            <td align="center">KEPALA BPMPTSP PROVINSI GORONTALO</td>
        </tr>
        <tr>
            <td><br><br><br><br><br></td>
        </tr>
        <tr>
            <td align="center"><u>Ir. HENRY F. DJUNA, MM</u></td>
        </tr>
        <tr>
            <td align="">NIP. 19570909 199203 1 001</td>
        </tr>

    </table>

<!--HALAMAN 2-->

<table width="100%">
    <TR>
        <TD ALIGN="CENTER" WIDTH="20%"><IMG SRC="<?=

            \yii\helpers\Url::to('@web/img/prov-gorontalo.jpg') ?>"
                                            WIDTH="13%"></TD>
        <TD ALIGN="CENTER" WIDTH="60%"><span style="font-size: 14pt">PEMERINTAH PROVINSI GORONTALO</span><BR>
            <span style="font-size: 16pt"><b>BADAN PENANAMAN MODAL DAN PTSP</b></span><BR>
            <span style="font-size: 8pt"><I>Jln. Tengah Desa Toto Selatan Kec. Kabila Kab. Bone Bolango Telp (0435) 8591278 Fax.
                    (0435) 8591277</span>
        </TD>
    </TR>
</table>
<hr style="border-top: 3px double #8c8b8b;">
<br>

<p style="font-size: 10pt; width: 90%;" align="center"><b>SURAT IZIN USAHA PERUSAHAAN BONGKAR MUAT (SIUPBM)  <br>
        NOMOR : <?= $model->pemohonDetail->no_terbit ?> <br>
    </b></p>
<p align="center">(Berdasarkan Peraturan Pemerintah Nomor 22 Tahun 2011 tentang Perubahan atas Peraturan Pemerintah Nomor 20 Tahun 2010 tentang Angkutan di Perairan)</p>
<p align="">Berdasarkan surat permohononan <?= $model->idPerusahaan->nama_perusahaan ?>, Nomor <?= $model->pemohonDetail->no_pemohon ?>, Tanggal <?= date('d-m-Y', strtotime($model->tgl_permohonan)); ?> Diberikan Surat Izin Usaha Perusahaan Bongkar Muat (SIUPBM) kepada :</p>
<br>
<table>
    <tr>
        <td>Nama Perusahaan</td>
        <td>: <?= $model->idPerusahaan->nama_perusahaan ?></td>
    </tr>
    <tr>
        <td>Alamat Perusahaan</td>
        <td>: <?= $model->idPerusahaan->jalan ?>, <?= $model->idPerusahaan->kelurahan0->nama_kel ?></td>
    </tr>
    <tr>
        <td>Nama Penanggung jawab</td>
        <td>: <?= $model->idPerusahaan->nama_pemilik_pj ?></td>
    </tr>
    <tr>
        <td>Nomor Pokok Wajib Pajak (NPWP)</td>
        <td>: <?= $model->idPerusahaan->npwp ?></td>
    </tr>
    <tr>
        <td>Status Perusahaan</td>
        <td>: <?= \app\components\ArrayData::bentukPerusahaan()[$model->idPerusahaan->bentuk]; ?></td>
    </tr>
</table>

<p style="font-weight: bold; margin-top: 20px;">Kewajiban Pemegang SIUPBM</p>
<ol>
    <li>Mematuhi seluruh peraturan perundang-undangan yang berlaku dibidang angkutan di perairan, kepelabuhan, dan lingkungan hidup.</li>
    <li>Bertanggung jawab atas kebenaran laporan kegiatan operasional perusahaan yang disampaikan kepada Gubernur Gorontalo, dengan tembusan kepada kantor kesyahbandaran dan Otoritas Pelabuhan (KSOP) dan atau Unit Penyelenggara Pelabuhan.</li>
    <li>Melaporkan secara tertulis kepada Gubernur Gorontalo, setiap kali terjadi perubahan maksud dan tujuan perusahaan, Direksi/Komisaris, nama dan alamat perusahaan, Nomor Pokok Wajib Pajak (NPWP) perusahaan dan kepemilikian peralatan bongkar muat.</li>
    <li>Menyampaikan Laporan bulan dan tahunan kegiatan operasional kepada Gubernur Gorontalo dengan tembusan kepada Direktur Jenderal Perhubungan Laut dan Kepala Dinas Perhubungan Provinsi Gorontalo.</li>
</ol>

<p>Surat Izin Usaha Perusahaan Bongkar Muat (SIUPBM) ini dapat dicabut apabila pemegang surat izin usaha tidak mematuhi kewajiban dalam surat izin usaha dan/atau melakukan tindak pidana yang bersangkutan dengan kegiatan usahanya dan perusahaan menyatakan membubarkan diri berdasarkan keputusan dari instansi yang berwenang.</p>
<p>SURAT IZIN USAHA ini berlaku untuk Pelabuhan Gorontalo diprovinsi Gorontalo selama perusahaan yang bersangkutan masih menjalankan usahanya.</p>

<p align="right" style="margin-top: 50px;"> Ditetapkan di Gorontalo <br>
    Pada tanggal, <?= DateIndo::createDate(DateIndo::FORMAT_BULAN_TEXT) ?></p>
<p align="right" style=""> </p>


<table align="right" width="30%">
    <tr style="border-spacing: 10px">
        <td align="center">a.n GUBERNUR GORONTALO</td>
    </tr>
    <tr>
        <td align="center">KEPALA BPMPTSP PROVINSI GORONTALO</td>
    </tr>
    <tr>
        <td><br><br><br><br><br></td>
    </tr>
    <tr>
        <td align="center"><u>Ir. HENRY F. DJUNA, MM</u></td>
    </tr>
    <tr>
        <td align="">NIP. 19570909 199203 1 001</td>
    </tr>

</table>

</body>
</html>