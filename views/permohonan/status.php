<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 23/02/16
 * Time: 15:30
 */

\app\assets\DataAsset::register($this);
$this->title = "Daftar Pengajuan permohonan";

?>


    <div class="box-header with-border">
        <div class="box-title">
            <?= $this->title ?>
        </div>

    </div>
    <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>No</th>
                <th>No Permohonan</th>
                <th>Nama Pemohon</th>
                <th>Nama Perusahaan</th>
                <th>Jenis Perizinan</th>
                <th>Tanggal</th>
                <th>Status permohonan</th>
                <th>Opsi</th>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; foreach($model as $m): ?>
            <tr>
                <td><?= $no++ ?></td>
                <td><?= $m->pemohonDetail->no_pemohon ?></td>
                <td><?= $m->idPengguna->nama ?></td>
                <td><?= $m->idPerusahaan->nama_perusahaan ?></td>
                <td><span title="<?= $m->idJenisPerizinan->nama_izin ?>"><?= $m->idJenisPerizinan->singkatan ?></span> </td>
                <td><?php
                    $datetime = date_create($m->tgl_permohonan) ;

                    echo date_format($datetime,"d-m-Y");
                    ?></td>
                <td><?php
                    $time = strtotime($m->tgl_terbit);
                    if($time){
                        echo 'Selesai';
                    }else{
                        echo 'Dalam Proses';
                    }
                    ?></td>
                <td>
                    <a class="btn btn-sm btn-info" href="<?= \yii\helpers\Url::to(['/permohonan/view/', 'id' => $m->id_pemohon]) ?>"><i class="fa fa-eye"></i></a>
                    <?php if(Yii::$app->user->can('admin')): ?>
                    <?= \yii\bootstrap\Html::a(
                        '',
                        ['permohonan/delete','id' => $m->id_pemohon],
                        [
                            'data-confirm'=>'Yakin ingin menghapus data ini??',
                            'method'=>'post',
                            'class'=> 'fa fa-remove btn btn-sm btn-danger']
                    ); ?>
                    <?php endif; ?>

                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
