<?php

use app\components\DateIndo;

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>

</head>
<body>


<table width="100%">
    <TR>
        <TD ALIGN="CENTER" WIDTH="20%"><IMG SRC="<?=

            \yii\helpers\Url::to('@web/img/prov-gorontalo.jpg') ?>"
                                            WIDTH="13%"></TD>
        <TD ALIGN="CENTER" WIDTH="60%"><span style="font-size: 16pt">PEMERINTAH PROVINSI GORONTALO</span><BR>
            <span style="font-size: 18pt"><b>BADAN PENANAMAN MODAL DAN PTSP</b></span><BR>
            <span style="font-size: 8pt"><I>Jln. Tengah Desa Toto Selatan Kec. Kabila Kab. Bone Bolango Telp (0435) 8591278 Fax.
                    (0435) 8591277</span>
        </TD>
    </TR>
</table>
<hr style="border-top: 3px double #8c8b8b;">
<BR>

<p style="font-size: 14pt;" align="center"><b>TANDA PENERIMAAN BERKAS PENGURUSAN IZIN <?= strtoupper($model->idJenisPerizinan->singkatan) ?></b></p>

<br>
<table width="100%" style="border-spacing: 18px;">
    <tr>
        <td width="30%">Nomor Permohonan</td>
        <td>: <?= $model->pemohonDetail->no_pemohon ?></td>
    </tr>
    <tr>
        <td>Ditujukan Kepada YTH</td>
        <td>: BPMPTSP Provinsi Gorontalo</td>
    </tr>
    <tr>
        <td>Tanggal Surat</td>
        <td>: <?= DateIndo::createDate(DateIndo::FORMAT_BULAN_TEXT) ?> </td>
    </tr>
    <tr>
        <td>Perihal</td>
        <td>: Permohonan Izin <?= strtoupper($model->idJenisPerizinan->singkatan) ?></td>
    </tr>
    <tr>
        <td>Contact Person/HP</td>
        <td>: <?= $model->idPengguna->telp ?></td>
    </tr>
</table>

<p align="right" style="margin-top: 50px;"> Gorontalo, <?= DateIndo::createDate(DateIndo::FORMAT_BULAN_TEXT) ?></p>


	<table align="right" width="30%">
        <tr style="border-spacing: 10px">
            <td align="center">Penerima</td>
        </tr>
        <tr>
            <td align="center">Front Office BPMPTSP Provinsi<br>Gorontalo </td>
        </tr>
        <tr>
            <td><br><br><br><br><br></td>
        </tr>

        <tr>
            <td align="center"><u>Jane G. D Supit, SS</u></td>
        </tr>
        <tr>
            <td align="">NIP. </td>
        </tr>

    </table>

<p style="margin-top: 50px;">Catatan :</p>
<ol>
    <li>Harap dicek kembali kesesuaian data-data yang tertulis pada permohonan</li>
    <li>BPMPTSP tidak bertanggung jawab atas kesalahan yang tertulis pada permohonan</li>
    <li>Waktu penyelesaian izin 1 (satu) hari kerja sejak tanggal diterimanya berkas permohonan secara lengkap dan benar</li>
    <li><b>TIDAK</b> dikenakan biaya dalam pengurusan ini</li>
    <li><b>Harap tanda terima ini dibawa serta pada saat pengambilan izin</b></li>
</ol>


</body>
</html>