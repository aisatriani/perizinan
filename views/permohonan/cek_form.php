<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 23/02/16
 * Time: 15:30
 */

use app\models\JenisPerizinan;
use yii\bootstrap\ActiveForm;

\app\assets\DataAsset::register($this);
$this->title = "Cek Pengajuan permohonan";

?>




    <div class="box-body">

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title"><?= $this->title ?></h3>

            </div><!-- /.box-header -->
            <div class="box-body" style="">
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                ]); ?>
                    <div class="form-group">
                        <label for="" class="col-md-2 control-label">No Permohonan</label>

                        <div class="col-md-4">
                            <?= \yii\bootstrap\Html::textInput('no_permohonan','',[
                                'class'=>'form-control typeahead',
                                'id'=>'no_permohonan',
                                'required'=>'true',
                                'placeholder'=>'masukan no permohonan'
                            ]);

                            ?>



                        </div>
                    </div>
                    
                    <div class="form-group">
                    	<div class="col-sm-10 col-sm-offset-2">
                    		<button type="submit" class="btn btn-primary">Cek</button>
                    	</div>
                    </div>




                <?php ActiveForm::end(); ?>
            </div><!-- /.box-body -->
        </div>



        <div id="content-details"></div>

        <?php if(Yii::$app->request->isPost): ?>

        <table id="example2" style="" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>No</th>
                <th>No Permohonan</th>
                <th>Nama Pemohon</th>
                <th>Nama Perusahaan</th>
                <th>Jenis Perizinan</th>
                <th>Tanggal</th>
                <th>Status permohonan</th>
                <th>Tanda Terima</th>
                <th>Opsi</th>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; foreach($model as $m): ?>
            <tr>
                <td><?= $no++ ?></td>
                <td><?= $m->pemohonDetail->no_pemohon ?></td>
                <td><?= $m->idPengguna->nama ?></td>
                <td><?= $m->idPerusahaan->nama_perusahaan ?></td>
                <td><?= $m->idJenisPerizinan->singkatan ?></td>
                <td><?php
                    $datetime = date_create($m->tgl_permohonan) ;
                    echo date_format($datetime,"d-m-Y");
                    ?></td>
                <td><?php
                    $time = strtotime($m->tgl_terbit);
                    if($time){
                        echo 'Selesai';
                    }else{
                        echo 'Dalam Proses';
                    }
                    ?></td>
                <td class="align-center">
                    <?= \yii\bootstrap\Html::a('',[
                        'permohonan/cetak-tanda',
                        'id'=> $m->id_pemohon
                    ],[
                        'class'=> 'fa fa-print btn btn-sm btn-info',
                        'title'=> 'cetak tanda terima',
                    ])
                    ?>
                </td>
                <td>
                    <a class="btn btn-sm btn-info" href="<?= \yii\helpers\Url::to(['/permohonan/view/', 'id' => $m->id_pemohon]) ?>"><i class="fa fa-eye"></i></a>
                    <?php if(Yii::$app->user->can('admin')): ?>
                    <?= \yii\bootstrap\Html::a(
                        ' ',
                        ['permohonan/delete','id' => $m->id_pemohon],
                        [
                            'class'=> 'fa fa-remove btn btn-sm btn-danger',
                            'data' => [
                                'confirm'=>'Yakin ingin menghapus data ini??',
                                'method'=>'post',
                            ]


                        ]
                    ); ?>
                    <?php endif; ?>

                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <?php endif; ?>

    </div>


<?php

$script = <<< JS

var substringMatcher = function(strs) {
  return function findMatches(q, cb) {
    var matches, substringRegex;

    // an array that will be populated with substring matches
    matches = [];

    // regex used to determine if a string contains the substring `q`
    substrRegex = new RegExp(q, 'i');

    // iterate through the pool of strings and for any string that
    // contains the substring `q`, add it to the `matches` array
    $.each(strs, function(i, str) {
      if (substrRegex.test(str)) {
        matches.push(str);
      }
    });

    cb(matches);
  };
};


var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
  'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
  'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
  'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
  'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
  'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
  'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
  'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
  'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
];

var nopemohon = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.whitespace,
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  // url points to a json file that contains an array of country names, see
  // https://github.com/twitter/typeahead.js/blob/gh-pages/data/countries.json
  prefetch: '/ajax/no-pemohon'
});


$('#no_permohonan').typeahead({
      minLength: 1,
      highlight: true
    },
    {
      name: 'nopemohon',
      source: nopemohon
      //source: function (query, process) {
      //  return $.get('/ajax/no-pemohon', { query: query }, function (data) {
      //      console.log(data);
      //      return process(data);
      //  });
      //}
});
JS;

$this->registerJsFile('@web/js/typeahead.js',['depends'=>[\app\assets\AppAsset::className()]]);
$this->registerJs($script);

