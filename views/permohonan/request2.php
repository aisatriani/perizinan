<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 23/02/16
 * Time: 21:26
 */
use app\components\ArrayData;
use app\models\JenisPerizinan;
use app\models\Pengguna;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;


/* @var $form ActiveForm */

$this->title = 'Form Pengajuan Izin';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Perizinan',
        'url' => ['admin/perizinan/view']
    ],
    'Form Perizinan',
];

$pengguna = Pengguna::findOne(Yii::$app->user->id);



?>


    <div class="box-header with-border">
        <?= $this->title ?>

    </div>

    <div class="box-body">

<!--            <form class="form-horizontal">-->
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal'
                ]); ?>


        <div class="row">
            <div class="col-sm-9 col-md-offset-1 ">
                <div class="box box-default">
                    <div class="box-header with-border">PERIZINAN</div>
                    <div class="box-body">


                        <?= $form->field($pemohon, 'id_jenis_perizinan')->dropDownList(
                            \yii\helpers\ArrayHelper::map(JenisPerizinan::find()->all(), 'id_izin', 'nama_izin'),
                            ['prompt' => '== PILIH JENIS IZIN == ','readonly'=> true]
                        ) ?>

                    </div>

                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-sm-9 col-md-offset-1 ">
                <div class="box box-default">
                    <div class="box-header with-border">DATA PEMOHON</div>
                    <div class="box-body">


                        <?= $form->field($pemohon, 'nama_pemohon')->textInput(['value' => $pengguna->nama, 'readonly' => 'true', 'class' => 'form-control spasi']); ?>
                        <?= $form->field($pemohon, 'alamat')->textInput(['value' => $pengguna->alamat, 'readonly' => 'true']); ?>

                    </div>

                </div>
            </div>
        </div>

        <div class="row" id="block-perusahaan">
            <div class="col-sm-9 col-md-offset-1 ">
                <div class="box box-default">
                    <div class="box-header with-border">DATA PERUSAHAN</div>
                    <div class="box-body">


                        <?= $form->field($perusahaan, 'nama_perusahaan')->textInput(['class' => 'form-control spasi']); ?>
                        <?= $form->field($perusahaan, 'nama_pemilik_pj')->textInput(['class' => 'form-control spasi']); ?>
                        <?= $form->field($perusahaan, 'id_provinsi')->dropDownList(
                            ArrayHelper::map(\app\models\Provinsi::find()->all(), 'id_prov','nama_prov'),
                            ['prompt'=>'Pilih Provinsi']
                        ) ?>
                        <?= $form->field($perusahaan, 'id_kabupaten')->dropDownList(
                        //ArrayHelper::map(\app\models\Kabupaten::find()->all(), 'id_kab','nama_kab'),
                            ['prompt'=>'Pilih Kabupaten/Kota']
                        ) ?>
                        <?= $form->field($perusahaan, 'id_kecamatan')->dropDownList(
                        //ArrayHelper::map(\app\models\Kabupaten::find()->all(), 'id_kab','nama_kab'),
                            ['prompt'=>'Pilih Kecamatan']
                        ) ?>
                        <?= $form->field($perusahaan, 'kelurahan')->dropDownList(
                        //ArrayHelper::map(\app\models\Kabupaten::find()->all(), 'id_kab','nama_kab'),
                            ['prompt'=>'Pilih Kelurahan']
                        ) ?>

                        <?= $form->field($perusahaan, 'jalan')->textInput(['class' => 'form-control spasi']); ?>
                        <?= $form->field($perusahaan, 'no')->textInput(['class' => 'form-control spasi']); ?>
                        <?= $form->field($perusahaan, 'npwp')->textInput(['class' => 'form-control spasi']); ?>
                        
                        <?= $form->field($perusahaan, 'bentuk')->dropDownList(
                            ArrayData::bentukPerusahaan(),
                            ['prompt'=>'Pilih Bentuk Usaha']
                        ) ?>


                    </div>

                </div>
            </div>
        </div>

        <div class="row" id="block-detail">
            <div class="col-sm-9 col-md-offset-1 ">
                <div class="box box-default">
                    <div class="box-header with-border">DATA DETAIL PERIZINAN</div>
                    <div class="box-body">

                        <?php foreach($kolom as $i=>$k): ?>

                            <div class="form-group field-dataisianperizinan-isian required">
                                <label class="control-label col-sm-3" for="DataIsianPerizinan-isian"><?= $k->nama_kolom ?></label>
                                <div class="col-sm-6">
<!--                                    <input type="text" id="perusahaan-npwp" class="form-control spasi" name="Perusahaan[npwp]">-->
                                    <?= Html::activeInput('text',$isian, "[$i]isian",['class' => 'form-control spasi']) ?>
                                    <?= Html::activeHiddenInput($isian, "[$i]id_data_kolom",['class' => 'form-control spasi','value'=> $k->id_kolom]) ?>
                                    <div class="help-block help-block-error "></div>
                                </div>

                            </div>

                        <?php endforeach; ?>

                    </div>

                </div>
            </div>
        </div>





                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-md-4 col-md-offset-4">
                        <button type="reset" class="btn btn-default btn80" id="reset">Batal</button>
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary btn80']) ?>
                    </div>

                </div>
                <!-- /.box-footer -->
        <?php ActiveForm::end(); ?>

    </div>

<?php

$script = <<< JS


    $('#perusahaan-id_provinsi').on('change',function(){
       var id_prov = this.value;
       var select_kabupaten = $("#perusahaan-id_kabupaten");
       $.get('/alamat/kabupaten?id_prov='+id_prov, function(data){
           select_kabupaten.html('');
           select_kabupaten.append(new Option('Pilih Kabupaten/Kota',''));
           $.each(data, function(i, item){

               select_kabupaten.append(new Option(item.nama_kab, item.id_kab));

           });
       })
   });

   $('#perusahaan-id_kabupaten').on('click',function(){
        var id_kab = this.value;
        var select_kecamatan = $('#perusahaan-id_kecamatan');
       $.get('/alamat/kecamatan?id_kab='+id_kab, function(data){
           select_kecamatan.html('');
           select_kecamatan.append(new Option('Pilih Kecamatan',''));
           $.each(data, function(i, item){

               select_kecamatan.append(new Option(item.nama_kec, item.id_kec));

           });
       });
   });

    $('#perusahaan-id_kecamatan').on('click',function(){
        var id_kec = this.value;
        var select_kelurahan = $('#perusahaan-kelurahan');
        $.get('/alamat/kelurahan?id_kec='+id_kec, function(data){
            select_kelurahan.html('');
            select_kelurahan.append(new Option('Pilih Kelurahan',''));
            $.each(data, function(i, item){

                select_kelurahan.append(new Option(item.nama_kel, item.id_kel));

            });
        });
    });

    $('#pemohon-id_jenis_perizinan').on('change', function(){
        //alert('data');
        $("#block-perusahaan").removeClass('hidden');
        $("#block-detail").removeClass('hidden');
    });

    $('#reset').on('click',function(){
        $("#block-perusahaan").addClass('hidden');
        $("#block-detail").addClass('hidden');
    });
JS;

$this->registerJs($script);



