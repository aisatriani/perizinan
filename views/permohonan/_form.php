<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pemohon */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pemohon-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_pengguna')->textInput() ?>

    <?= $form->field($model, 'id_perusahaan')->textInput() ?>

    <?= $form->field($model, 'id_jenis_perizinan')->textInput() ?>

    <?= $form->field($model, 'tgl_permohonan')->textInput() ?>

    <?= $form->field($model, 'tgl_terbit')->textInput() ?>

    <?= $form->field($model, 'verifikasi')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
