<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 23/02/16
 * Time: 15:30
 */

use app\models\JenisPerizinan;
use yii\bootstrap\ActiveForm;

\app\assets\DataAsset::register($this);
$this->title = "Daftar Pengajuan permohonan";

?>


    <div class="box-header with-border">
        <div class="box-title">
            <?= $this->title ?>
        </div>

    </div>

    <div class="box-body">

        <div class="box box-success collapsed-box">
            <div class="box-header with-border">
                <h3 class="box-title">Tampil Berdasarkan :</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body" style="display: none;">
                <form action="" method="post" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="" class="col-md-3 control-label">Tampil Berdasarkan Izin</label>

                        <div class="col-md-4">
                            <?= \yii\bootstrap\Html::dropDownList(
                                'nama_izin',
                                null,
                                \yii\helpers\ArrayHelper::map(JenisPerizinan::find()->all(), 'id_izin','nama_izin'),
                                [
                                    'prompt'=>'Pilih Jenis Perizinan',
                                    'onchange'=>'this.form.submit()',
                                    'class'=> 'form-control input-sm'
                                ]
                            ) ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-md-3 control-label">Tampil Berdasarkan Status</label>

                        <div class="col-md-4">
                            <?= \yii\bootstrap\Html::dropDownList(
                                'status',
                                null,
                                [
                                    '0' => 'Dalam Proses',
                                    '1' => 'Terbit',
                                ],
                                [
                                    'prompt'=>'Pilih Status Permohonan',
                                    'onchange'=>'this.form.submit()',
                                    'class'=> 'form-control input-sm'
                                ]
                            ) ?>
                        </div>
                    </div>


                </form>
            </div><!-- /.box-body -->
        </div>






        <table id="example2" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>No</th>
                <th>No Permohonan</th>
                <th>Nama Pemohon</th>
                <th>Nama Perusahaan</th>
                <th>Jenis Perizinan</th>
                <th>Tanggal</th>
                <th>Status permohonan</th>
                <th>Opsi</th>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; foreach($model as $m): ?>
            <tr>
                <td><?= $no++ ?></td>
                <td><?= $m->pemohonDetail->no_pemohon ?></td>
                <td><?= $m->idPengguna->nama ?></td>
                <td><?= $m->idPerusahaan->nama_perusahaan ?></td>
                <td><?= $m->idJenisPerizinan->singkatan ?></td>
                <td><?php
                    $datetime = date_create($m->tgl_permohonan) ;

                    echo date_format($datetime,"d-m-Y");
                    ?></td>
                <td><?php
                    $time = strtotime($m->tgl_terbit);
                    if($time){
                        echo 'Selesai';
                    }else{
                        echo 'Dalam Proses';
                    }
                    ?></td>
                <td>
                    <a class="btn btn-sm btn-info" href="<?= \yii\helpers\Url::to(['/permohonan/view/', 'id' => $m->id_pemohon]) ?>"><i class="fa fa-eye"></i></a>
                    <?php if(Yii::$app->user->can('admin')): ?>
                    <?= \yii\bootstrap\Html::a(
                        ' ',
                        ['permohonan/delete','id' => $m->id_pemohon],
                        [
                            'class'=> 'fa fa-remove btn btn-sm btn-danger',
                            'data' => [
                                'confirm'=>'Yakin ingin menghapus data ini??',
                                'method'=>'post',
                            ]


                        ]
                    ); ?>
                    <?php endif; ?>

                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
