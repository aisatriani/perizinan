<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PemohonSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pemohon-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_pemohon') ?>

    <?= $form->field($model, 'id_pengguna') ?>

    <?= $form->field($model, 'id_perusahaan') ?>

    <?= $form->field($model, 'id_jenis_perizinan') ?>

    <?= $form->field($model, 'tgl_permohonan') ?>

    <?php // echo $form->field($model, 'tgl_terbit') ?>

    <?php // echo $form->field($model, 'verifikasi') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
