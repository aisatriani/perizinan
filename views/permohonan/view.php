<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pemohon */
/* @var $isian app\models\DataIsianPerizinan */

$this->title = 'Permohonan '.strtoupper($model->idJenisPerizinan->singkatan) ;
$this->params['breadcrumbs'][] = ['label' => 'Permohonan', 'url' => ['list']];
$this->params['breadcrumbs'][] = $this->title;

$time = strtotime($model->tgl_terbit);

?>
<div class="pemohon-view box-body">

    <div class="box-header with-border">
        <div class="box-title">
            <?= 'Permohonan '.strtoupper($model->idJenisPerizinan->singkatan) ; ?>
        </div>
    </div>

    <p>

        <?= Html::a('< Back', Yii::$app->request->referrer, ['class' => 'btn btn-primary']) ?>

        <?php if(Yii::$app->user->can('admin') || Yii::$app->user->can('eksekutor')): ?>

            <?php if($time): ?>

                <?= \yii\bootstrap\Html::a(
                    ' Terbitkan',
                    ['eksekutor/terbit','id' => $model->id_pemohon],
                    [
                        'data-confirm'=>'Data ini telah disetujui dan akan diterbitkan segera ??',
                        'method'=>'post',
                        'class'=> 'btn btn-warning']
                ); ?>

            <?php endif; ?>




        <?php endif; ?>


        <?php if(Yii::$app->user->can('admin')): ?>


        <?= Html::a('Hapus', ['delete', 'id' => $model->id_pemohon], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>


    <?php endif; ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idPengguna.nik',
            'idPengguna.nama',
            'idPengguna.alamat',
            //'id_pengguna',
            //'id_perusahaan',
            'idPerusahaan.nama_perusahaan',
            'idPerusahaan.nama_pemilik_pj',
            'idPerusahaan.kelurahan0.nama_kel',
            'idJenisPerizinan.nama_izin',
            'tgl_permohonan',
//            'tgl_terbit',
//            'verifikasi',
//            'created_at',
//            [
//                'attribute' => 'dataIsianPerizinans',
//                'value' => $model->getDescriptionText(),
//            ]

        ],
    ]);

//    foreach($isian as $isi):
//
//        echo DetailView::widget([
//            'model' => $isi,
//            'attributes' => [
//                //'isian',
//                [
//                    'label' => $isi->idDataKolom->nama_kolom,
//                    'value' => $isi->isian,
//                ]
//
//            ],
//        ]);
//
//    endforeach;


    ?>


    <div class="box-header with-border">
        <div class="box-title">Detail Perizinan</div>

    </div>


    <table id="w1" class="table table-striped table-bordered detail-view">
        <?php foreach($isian as $isi): ?>
            <tr><th><?=$isi->idDataKolom->nama_kolom ?></th><td><?= $isi->isian ?></td></tr>
        <?php endforeach; ?>

    </table>

</div>
