<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 23/02/16
 * Time: 21:26
 */
use app\components\ArrayData;
use app\models\JenisPerizinan;
use app\models\Pengguna;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;


/* @var $form ActiveForm */

$this->title = 'Form Pengajuan Izin';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Perizinan',
        'url' => ['admin/perizinan/view']
    ],
    'Form Perizinan',
];

$pengguna = Pengguna::findOne(Yii::$app->user->id);



?>


    <div class="box-header with-border">
        <?= $this->title ?>

    </div>

    <div class="box-body">

<!--            <form class="form-horizontal">-->
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'action' => ['permohonan/izindetail'],
                ]); ?>


        <div class="row">
            <div class="col-sm-9 col-md-offset-1 ">
                <div class="box box-default">
                    <div class="box-header with-border">PERIZINAN</div>
                    <div class="box-body">


                        <?= $form->field($pemohon, 'id_jenis_perizinan')->dropDownList(
                            \yii\helpers\ArrayHelper::map(JenisPerizinan::find()->all(), 'id_izin', 'nama_izin'),
                            ['prompt' => '== PILIH JENIS IZIN == ']
                        ) ?>

                    </div>

                </div>
            </div>
        </div>









                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-md-4 col-md-offset-4">
                        <button type="reset" class="btn btn-default btn80" id="reset">Batal</button>
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary btn80']) ?>
                    </div>

                </div>
                <!-- /.box-footer -->
        <?php ActiveForm::end(); ?>

    </div>

<?php

$script = <<< JS


    $('#perusahaan-id_provinsi').on('change',function(){
       var id_prov = this.value;
       var select_kabupaten = $("#perusahaan-id_kabupaten");
       $.get('/alamat/kabupaten?id_prov='+id_prov, function(data){
           select_kabupaten.html('');
           select_kabupaten.append(new Option('Pilih Kabupaten/Kota',''));
           $.each(data, function(i, item){

               select_kabupaten.append(new Option(item.nama_kab, item.id_kab));

           });
       })
   });

   $('#perusahaan-id_kabupaten').on('click',function(){
        var id_kab = this.value;
        var select_kecamatan = $('#perusahaan-id_kecamatan');
       $.get('/alamat/kecamatan?id_kab='+id_kab, function(data){
           select_kecamatan.html('');
           select_kecamatan.append(new Option('Pilih Kecamatan',''));
           $.each(data, function(i, item){

               select_kecamatan.append(new Option(item.nama_kec, item.id_kec));

           });
       });
   });

    $('#perusahaan-id_kecamatan').on('click',function(){
        var id_kec = this.value;
        var select_kelurahan = $('#perusahaan-kelurahan');
        $.get('/alamat/kelurahan?id_kec='+id_kec, function(data){
            select_kelurahan.html('');
            select_kelurahan.append(new Option('Pilih Kelurahan',''));
            $.each(data, function(i, item){

                select_kelurahan.append(new Option(item.nama_kel, item.id_kel));

            });
        });
    });

    $('#pemohon-id_jenis_perizinan').on('change', function(){
        //alert('data');
        $("#block-perusahaan").removeClass('hidden');
        $("#block-detail").removeClass('hidden');
    });

    $('#reset').on('click',function(){
        $("#block-perusahaan").addClass('hidden');
        $("#block-detail").addClass('hidden');
    });
JS;

$this->registerJs($script);



