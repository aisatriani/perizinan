<?php

/* @var $this yii\web\View */

$this->title = 'Elektronik Pelayanan Perizinan Terpadu';
?>

<div class="site-index">

    <div class="jumbotron">
        <h3>SELAMAT DATANG DI PERIZINAN ONLINE PROVINSI GORONTALO </h3>

        <p class="lead">Sistem Perizinan ini di peruntukan bagi pemohon yang ingin mengurus permohonan secara online.</p>

        <?php if(Yii::$app->user->isGuest){ ?>
        <p><a class="btn btn-lg btn-success" href="<?= \yii\helpers\Url::to(['site/register']) ?>">Daftar Sekarang</a></p>
        <?php }else { ?>
        <p><a class="btn btn-lg btn-success" href="<?= \yii\helpers\Url::to(['permohonan/izin']) ?>">Ajukan Sekarang</a></p>
        <?php } ?>
    </div>


</div>

