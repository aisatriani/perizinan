<?php

/* @var $this yii\web\View */

$this->title = 'Syarat dan ketentuan permohonan perizinan';
?>

<div class="site-index">
<div class="container">



        <p><strong>1. IZIN USAHA BONGKAR MUAT DI PELABUHAN</strong></p>
        <p>A. PERSYARATAN</p>
        <p>Administrasi</p>
        <ol>
            <li>Memiliki Akta Pendirian Perusahaan (BHI)</li>
            <li>Memiliki NPWP Perusahaan</li>
            <li>Memiliki modal usaha</li>
            <li>Memiliki penanggung jawab</li>
            <li>Menenmpati tempat usaha, baik milik sendiri maupn sewa, berdasarkan surat keterangan domisili perusahaan dari instansi yang berwenang</li>
            <li>Memiliki tenaga ahli dan kualifikasi nautika atau ahli ketatalaksanaan pelayanan niaga</li>
        </ol>
        <p>Teknis (Disesuaikan dengan pelabuhan)</p>
        <p>B. WAKTU PENYELESAIAN PELAYANAN</p>
        <p>C. BIAYA PELAYANAN</p>
        <p>Tidak dikenakan biaya</p>
        <p>&nbsp;</p>
        <p><strong>2. IZIN USAHA JASA PENGURUSAN TRANSPORTASI (JPT)</strong></p>
        <p>A. PESYARATAN</p>
        <p>Adminisrasi</p>
        <ol>
            <li>Memiliki Akta Pendirian Perusahaan</li>
            <li>Memiliki Nomor Pokok Wajib Pajak Perusahaan</li>
            <li>Memiliki Modal Usaha</li>
            <li>Memiliki Penanggung Jawab</li>
            <li>Memiliki peralatan yang cukup sesuai dengan perkembangan teknologi</li>
            <li>Memiliki tenaga ahli yang sesuai</li>
            <li>Memiliki surat keterangan domisili perusahaan</li>
        </ol>
        <p>B. WAKTU PENYELESAIAN PELAYANAN</p>
        <p>3 (tiga) hari kerja sejak tanggal diterimanya berkas permohonan secara lengkap dan benar</p>
        <p>C. BIAYA PELAYANAN</p>
        <p>TIdak dikenakan biaya</p>
        <p>&nbsp;</p>
        <p><strong>3. IZIN TRAYEK AKDP</strong></p>
        <p>A. PERSAYARATAN</p>
        <p>Administrasi</p>
        <ol>
            <li>Fotocopy KTP Pemilik</li>
            <li>Fotocopy STNKB</li>
            <li>Fotocopy Buku Kir yang masih berlaku</li>
            <li>Fotocopy Jasa Raharja</li>
            <li>Rekomendasi Izin Trayek Asal Tujuan</li>
            <li>Kenderaan tersebut layak operasional</li>
        </ol>
        <p>B. WAKTU PENYELESAIAN PELAYANAN</p>
        <ul>
            <li>Untuk perpanjangan Kartu Pengawasan (KP)&nbsp;&plusmn;5 &ndash; 10 menit jika berkas permohonan sudah lengkap dan benar</li>
            <li>Untuk pengurusan Izin Trayek Baru selama 1 (satu) hari kerja sejak tanggal diterimanya berkas permohonan secara lengkap dan benar</li>
        </ul>
        <p>C. BIAYA PELAYANAN</p>
        <p>Tidak dikenakan biaya</p>
        <p>&nbsp;</p>

</div>

</div>



