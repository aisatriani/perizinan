<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 23/02/16
 * Time: 21:26
 */
use app\models\LevelPengguna;
use app\models\Pengguna;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;


/* @var $form ActiveForm */
/* @var $model Pengguna */

#$this->title = 'Form Tambah Perizinan';
$this->params['breadcrumbs'] = [

    'Daftar',
];



?>


    <div class="box-header with-border">
        <?= $this->title ?>

    </div>

    <div class="box-body">

<!--            <form class="form-horizontal">-->
                <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
                <?php //echo $form->errorSummary($model); ?>
                <div class="box-body">


                        <?= $form->field($model, 'nik') ?>
                        <?= $form->field($model, 'nama') ?>
                        <?= $form->field($model, 'email') ?>
                        <?= $form->field($model, 'username') ?>
                        <?= $form->field($model, 'password')->passwordInput() ?>
                        <?= $form->field($model, 'password_repeat')->passwordInput() ?>


                    <div class="form-group">
                        <div class="col-md-12 col-md-offset-1">
                            <div class="box-header with-border col-sm-3">
                                Detil Alamat

                            </div>
                        </div>
                    </div>

                        <?= $form->field($model, 'id_provinsi')->dropDownList(
                            ArrayHelper::map(\app\models\Provinsi::find()->all(), 'id_prov','nama_prov'),
                            ['prompt'=>'Pilih Provinsi']
                        ) ?>
                        <?= $form->field($model, 'id_kabupaten')->dropDownList(
                            //ArrayHelper::map(\app\models\Kabupaten::find()->all(), 'id_kab','nama_kab'),
                            ['prompt'=>'Pilih Kabupaten/Kota']
                        ) ?>
                        <?= $form->field($model, 'id_kecamatan')->dropDownList(
                        //ArrayHelper::map(\app\models\Kabupaten::find()->all(), 'id_kab','nama_kab'),
                            ['prompt'=>'Pilih Kecamatan']
                        ) ?>
                        <?= $form->field($model, 'id_kelurahan')->dropDownList(
                        //ArrayHelper::map(\app\models\Kabupaten::find()->all(), 'id_kab','nama_kab'),
                            ['prompt'=>'Pilih Kelurahan']
                        ) ?>
                        <?= $form->field($model, 'alamat') ?>
                        <?= $form->field($model, 'telp') ?>




                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-md-4 col-md-offset-4">
                        <button type="reset" class="btn btn-default btn80">Reset</button>
                        <?= Html::submitButton('Daftar', ['class' => 'btn btn-primary btn80']) ?>
                    </div>

                </div>
                <!-- /.box-footer -->
        <?php ActiveForm::end(); ?>

    </div>


