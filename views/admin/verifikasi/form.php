<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 23/02/16
 * Time: 21:26
 */
use app\models\JenisPerizinan;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;


/* @var $form ActiveForm */

#$this->title = 'Form Detil Perizinan';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Verifikasi',
        'url' => ['admin/verifikasi/view']
    ],
    'Form Verifikasi',
];



?>

    <div class="box-header with-border">
        <?= $this->title ?>

    </div>

    <div class="box-body">

<!--            <form class="form-horizontal">-->
                <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
                <div class="box-body">


                        <?= $form->field($model, 'id_perizinan')->dropDownList(
                            ArrayHelper::map(JenisPerizinan::find()->all(),'id_izin','nama_izin'),
                            ['prompt' => 'Pilih Jenis Perizinan']
                        ) ?>



                        <?= $form->field($model, 'nama_verifikasi') ?>



                        <?= $form->field($model, 'keterangan')->textarea() ?>




                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-md-4 col-md-offset-4">
                        <button type="reset" class="btn btn-default btn80">Reset</button>
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary btn80']) ?>
                    </div>

                </div>
                <!-- /.box-footer -->
        <?php ActiveForm::end(); ?>

    </div>

