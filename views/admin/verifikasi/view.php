<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 23/02/16
 * Time: 15:30
 */

/* @var $m \app\models\DataKolomPerizinan */

\app\assets\DataAsset::register($this);
$this->title = "Detil Perizinan";

?>


    <div class="box-header with-border">
        <?= $this->title ?>
        <a class="pull-right btn btn-flat btn-primary" href="<?= \yii\helpers\Url::to(['admin/verifikasi/create']) ?>"><i class="fa fa-plus"></i> Tambahkan</a>
    </div>
    <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>No</th>
                <th>Perizinan</th>
                <th>Nama Verifikasi</th>
                <th>Keterangan</th>
                <th>Opsi</th>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; foreach($model as $m): ?>
            <tr>
                <td><?= $no++ ?></td>
                <td><?= $m->idPerizinan->singkatan ?></td>
                <td><?= $m->nama_verifikasi ?></td>
                <td><?= \yii\helpers\StringHelper::truncate($m->keterangan, 50);  ?></td>

                <td>
                    <a class="btn btn-sm btn-info" href="<?= \yii\helpers\Url::to(['admin/verifikasi/edit/', 'pid' => $m->id_verifikasi]) ?>"><i class="fa fa-pencil"></i></a>
                    <?= \yii\bootstrap\Html::a(
                        '',
                        ['admin/verifikasi/delete','pid' => $m->id_verifikasi],
                        [
                            'data-confirm'=>'Yakin ingin menghapus data ini??',
                            'class'=> 'fa fa-remove btn btn-sm btn-danger']
                    ); ?>

                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
