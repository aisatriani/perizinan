<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 23/02/16
 * Time: 21:26
 */
use app\models\LevelPengguna;
use app\models\Pengguna;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;


/* @var $form ActiveForm */
/* @var $model Pengguna */


$this->params['breadcrumbs'] = [
    [
        'label' => 'Pengguna',
        'url' => ['admin/pengguna/view']
    ],
    [
        'label' => 'Tingkat Pengguna',
        'url' => ['admin/pengguna/level/view']
    ],
    'Form Tingkatan Pengguna',
];



?>



    <div class="box-header with-border">
        <?= $this->title ?>

    </div>

    <div class="box-body">

<!--            <form class="form-horizontal">-->
                <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
                <?php //echo $form->errorSummary($model); ?>
                <div class="box-body">


                        <?= $form->field($model, 'nama_level') ?>
                        <?= $form->field($model, 'keterangan') ?>




                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-md-4 col-md-offset-4">
                        <button type="reset" class="btn btn-default btn80">Reset</button>
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary btn80']) ?>
                    </div>

                </div>
                <!-- /.box-footer -->
        <?php ActiveForm::end(); ?>

    </div>

