<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 23/02/16
 * Time: 15:30
 */

\app\assets\DataAsset::register($this);
$this->title = "Daftar Pengguna";

?>


    <div class="box-header with-border">
        <div class="box-title">
            <?= $this->title ?>
        </div>

        <a class="pull-right btn btn-flat btn-primary" href="<?= \yii\helpers\Url::to(['admin/pengguna/create']) ?>"><i class="fa fa-plus"></i> Tambahkan</a>
    </div>
    <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>No</th>
                <th>NIK</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Username</th>
                <th>Alamat</th>
                <th>Telp</th>
                <th>Opsi</th>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; foreach($model as $m): ?>
            <tr>
                <td><?= $no++ ?></td>
                <td><?= $m->nik ?></td>
                <td><?= $m->nama ?></td>
                <td><?= $m->email ?></td>
                <td><?= $m->username ?></td>
                <td><?= $m->alamat ?></td>
                <td><?= $m->telp ?></td>
                <td>
                    <a class="btn btn-sm btn-info" href="<?= \yii\helpers\Url::to(['admin/pengguna/edit/', 'pid' => $m->id_pengguna]) ?>"><i class="fa fa-pencil"></i> Edit</a>
                    <?= \yii\bootstrap\Html::a(
                        ' Hapus',
                        ['admin/pengguna/delete','pid' => $m->id_pengguna],
                        [
                            'data-confirm'=>'Yakin ingin menghapus data ini??',
                            'class'=> 'fa fa-remove btn btn-sm btn-danger']
                    ); ?>

                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
