<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 23/02/16
 * Time: 12:37
 */
use yii\helpers\Html;

$this->title = 'Admin';
$this->params['breadcrumbs'][] = $this->title;

?>


    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
</div>

<div class="box-body">
    This is the About page. You may modify the following file to customize its content:
    <code><?= __FILE__ ?></code>
</div>

