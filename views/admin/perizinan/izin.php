<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 23/02/16
 * Time: 15:30
 */

\app\assets\DataAsset::register($this);
$this->title = "Jenis Perizinan";

?>

    <div class="box-header with-border">
        <?= $this->title ?>
        <a class="pull-right btn btn-flat btn-primary" href="<?= \yii\helpers\Url::to(['admin/perizinan/create']) ?>"><i class="fa fa-plus"></i> Tambahkan</a>
    </div>
    <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>No</th>
                <th>Nama Perizinan</th>
                <th>Singkatan</th>
                <th>Keterangan</th>
                <th>Opsi</th>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; foreach($model as $m): ?>
            <tr>
                <td><?= $no++ ?></td>
                <td><?= $m->nama_izin ?></td>
                <td><?= $m->singkatan ?></td>
                <td><?= $m->keterangan ?></td>
                <td>
                    <a class="btn btn-sm btn-info" href="<?= \yii\helpers\Url::to(['admin/perizinan/edit/', 'pid' => $m->id_izin]) ?>"><i class="fa fa-pencil"></i> Edit</a>
                    <?= \yii\bootstrap\Html::a(
                        ' Hapus',
                        ['admin/perizinan/delete','pid' => $m->id_izin],
                        [
                            'data-confirm'=>'Yakin ingin menghapus data ini??',
                            'class'=> 'fa fa-remove btn btn-sm btn-danger']
                    ); ?>

                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>

