<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 23/02/16
 * Time: 21:26
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;


/* @var $form ActiveForm */

#$this->title = 'Form Tambah Perizinan';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Perizinan',
        'url' => ['admin/perizinan/view']
    ],
    'Form Perizinan',
];



?>


    <div class="box-header with-border">
        <?= $this->title ?>

    </div>

    <div class="box-body">

<!--            <form class="form-horizontal">-->
                <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
                <div class="box-body">

                    <div class="form-group">
                        <?= $form->field($model, 'nama_izin') ?>
                    </div>

                    <div class="form-group">
                        <?= $form->field($model, 'singkatan') ?>
                    </div>

                    <div class="form-group">
                        <?= $form->field($model, 'keterangan') ?>
                    </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-md-4 col-md-offset-4">
                        <button type="reset" class="btn btn-default btn80">Reset</button>
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary btn80']) ?>
                    </div>

                </div>
                <!-- /.box-footer -->
        <?php ActiveForm::end(); ?>

    </div>

