<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 23/02/16
 * Time: 15:30
 */

/* @var $m \app\models\DataKolomPerizinan */

\app\assets\DataAsset::register($this);
$this->title = "Detil Perizinan";

?>

    <div class="box-header with-border">
        <?= $this->title ?>
        <a class="pull-right btn btn-flat btn-primary" href="<?= \yii\helpers\Url::to(['admin/perizinan/detil/create']) ?>"><i class="fa fa-plus"></i> Tambahkan</a>
    </div>
    <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>No</th>
                <th>Perizinan</th>
                <th>Nama Kolom</th>
                <th>Keterangan</th>
                <th>Opsi</th>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; foreach($model as $m): ?>
            <tr>
                <td><?= $no++ ?></td>
                <td><?= $m->idPerizinan->singkatan ?></td>
                <td><?= $m->nama_kolom ?></td>
                <td><?= $m->keterangan ?></td>

                <td>
                    <a class="btn btn-sm btn-info" href="<?= \yii\helpers\Url::to(['admin/perizinan/detil/edit/', 'pid' => $m->id_kolom]) ?>"><i class="fa fa-pencil"></i> Edit</a>
                    <?= \yii\bootstrap\Html::a(
                        ' Hapus',
                        ['admin/perizinan/detil/delete','pid' => $m->id_kolom],
                        [
                            'data-confirm'=>'Yakin ingin menghapus data ini??',
                            'class'=> 'fa fa-remove btn btn-sm btn-danger']
                    ); ?>

                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
