<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

//$model = \app\models\Pengguna::findByUsername(Yii::$app->user->identity->username);
//if (!Yii::$app->user->isGuest)
//    echo 'login users';
//?>

<header class="main-header">


    <nav class="navbar navbar-static-top">

        <div class="container">

            <div class="navbar-header">
                <a href="<?= Yii::$app->homeUrl ?>" class="navbar-brand"><b><?= Yii::$app->name ?></b></a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav">



<!--                    <li class="active">--><?//= Html::a('Alur Proses Permohonan', ['pemohon/alur']) ?><!--</li>-->

                        <?php if(!Yii::$app->user->isGuest): ?>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= Yii::$app->user->identity->username ?>, akun saya <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><?= Html::a('Ajukan Permohonan',['/permohonan/izin']); ?></li>
                            <li><?= Html::a('Status Permohonan',['/permohonan/status']); ?></li>

                        </ul>
                    </li>
                        <?php endif; ?>





                    <?php if(Yii::$app->user->can('admin') || Yii::$app->user->can('fo')): ?>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">FO<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><?= Html::a('Ajukan Permohonan',['/permohonan/izin']); ?></li>
                            <li><?= Html::a('Daftar Pengajuan Permohonan izin',['/permohonan/list']); ?></li>
                            <li><?= Html::a('Cek Status Permohonan',['/permohonan/cek']); ?></li>

                            <li class="divider"></li>
                            <li><?= Html::a('Informasi Syarat Ketentuan perizinan',['/site/syarat']); ?></li>
                        </ul>
                    </li>

                    <?php endif; ?>


                    <?php if(Yii::$app->user->can('admin') || Yii::$app->user->can('tim-teknis')): ?>


                        <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tim Teknis<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><?= Html::a('Validasi Berkas',['/validator']); ?></li>
                            <li><?= Html::a('Daftar Berkas Koreksi',['/validator/koreksi']); ?></li>
                            <li><?= Html::a('Daftar Berkas Verifikasi',['/validator/verifikasi']); ?></li>
                            <li><?= Html::a('Daftar Pengajuan Permohonan izin',['/permohonan/list']); ?></li>
                            <li><?= Html::a('Cetak Surat Izin',['/permohonan/terbit']); ?></li>
                            <li class="divider"></li>
                            <li><?= Html::a('Informasi Syarat Ketentuan perizinan',['/site/syarat']); ?></li>
                        </ul>
                    </li>

                    <?php endif; ?>


                    <?php if(Yii::$app->user->can('admin') || Yii::$app->user->can('korektor')): ?>


                        <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Korektor<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><?= Html::a('Koreksi Berkas',['/koreksi']); ?></li>
                            <li><?= Html::a('Daftar Pengajuan Permohonan izin',['/permohonan/list']); ?></li>
                            <li class="divider"></li>
                            <li><?= Html::a('Informasi Syarat Ketentuan perizinan',['/site/syarat']); ?></li>
                        </ul>
                    </li>

                    <?php endif; ?>

                    <?php if(Yii::$app->user->can('admin') || Yii::$app->user->can('verifikator')): ?>


                        <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Verifikator<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><?= Html::a('Verifikasi Berkas',['/verifikasi']); ?></li>
                            <li><?= Html::a('Daftar Pengajuan Permohonan izin',['/permohonan/list']); ?></li>
                            <li class="divider"></li>
                            <li><?= Html::a('Informasi Syarat Ketentuan perizinan',['/site/syarat']); ?></li>
                        </ul>
                    </li>

                    <?php endif; ?>


                    <?php if(Yii::$app->user->can('admin') || Yii::$app->user->can('eksekutor')): ?>


                        <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Eksekutor<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><?= Html::a('Eksekusi Berkas',['/eksekutor']); ?></li>
                            <li><?= Html::a('Daftar Pengajuan Permohonan izin',['/permohonan/list']); ?></li>
                            <li class="divider"></li>
                            <li><?= Html::a('Informasi Syarat Ketentuan perizinan',['/site/syarat']); ?></li>
                        </ul>
                    </li>

                    <?php endif; ?>


                    <?php if(Yii::$app->user->can('admin')): ?>


                        <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pengaturan <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?= \yii\helpers\Url::to(['/admin/perizinan/view']) ?>">Jenis Perizinan</a></li>
                            <li><a href="<?= \yii\helpers\Url::to(['/admin/perizinan/detil/view']) ?>">Detil Perizinan</a></li>
                            <li><?= Html::a('Detil Verifikasi',['/admin/verifikasi/view']) ?></li>
                            <li class="divider"></li>
                            <li><a href="<?= \yii\helpers\Url::to(['/admin/pengguna/view']) ?>">Pengguna</a></li>
                            <li><a href="<?php echo \yii\helpers\Url::to(['/mimin/route']) ?>">Atur Akses Rute</a></li>
                            <li><a href="<?php echo \yii\helpers\Url::to(['/mimin/role']) ?>">Atur Akses Pengguna</a></li>
                            <li><a href="<?php echo \yii\helpers\Url::to(['/mimin/user']) ?>">Berikan Akses Pengguna</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Opsi</a></li>
                        </ul>
                    </li>
                    <?php  endif; ?>

                    <?php if(!Yii::$app->user->can('admin')): ?>

                    <li class="dropdown">
                        <a href="<?= \yii\helpers\Url::to('/site/alur') ?>" <span class="fa fa-exchange"> </span> Alur Permohonan</a>
                        <ul class="dropdown-menu" role="menu">
                            <li><?= Html::a('Pengurusan Izin Baru',['/alur/baru']); ?></li>
                            <li><?= Html::a('Perpanjangan Izin',['/alur/perpanjangan']); ?></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-tachometer"></span> Informasi <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><?= Html::a('Syarat dan ketentuan',['/site/syarat']); ?></li>
                            <li><?= Html::a('Tentang Aplikasi',['/site/about']); ?></li>
                        </ul>
                    </li>

                    <?php endif; ?>

                    <?php if(Yii::$app->user->isGuest): ?>
                    <li><?= Html::a(" Masuk",['/site/login'],['class'=> 'fa fa-lock']) ?></li>
                    <?php endif; ?>

                </ul>

            </div>
            <!-- /.navbar-collapse -->
            <!-- Navbar Right Menu -->
            <?php if(!Yii::$app->user->isGuest): ?>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->

                    <!-- /.messages-menu -->

                    <!-- Notifications Menu -->

                    <!-- Tasks Menu -->

                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs"><?php echo Yii::$app->user->identity->nama;  ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                                <p>
                                    <?= Yii::$app->user->identity->username ?>
                                    <small>welcome back,</small>
                                </p>
                            </li>
                            <!-- Menu Body -->

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
<!--                                    <a href="#" class="btn btn-default btn-flat">Profile</a>-->
                                </div>
                                <div class="pull-right">
<!--                                    <a href="#" class="btn btn-default btn-flat">Sign out</a>-->
                                    <?php
                                    if(!Yii::$app->user->isGuest):
                                        echo Html::beginForm(['/site/logout'], 'post')
                                        . Html::submitButton('Logout (' . Yii::$app->user->identity->username . ')',
                                        ['class' => 'btn btn-default btn-falt'])
                                        . Html::endForm();
                                    endif;
                                    ?>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <?php endif; ?>
            <!-- /.navbar-custom-menu -->
        </div>
        <!-- /.container-fluid -->
    </nav>
</header>