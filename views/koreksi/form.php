<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 23/02/16
 * Time: 21:26
 */
use app\components\ArrayData;
use app\models\JenisPerizinan;
use app\models\Pengguna;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;


/* @var $form ActiveForm */
/* @var $verisian \app\models\VerifikasiIsian */
/* @var $validasi \app\models\Verifikasi */
\app\assets\DataAsset::register($this);

$this->title = 'Form Koreksi Berkas';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Koreksi',
        'url' => ['koreksi/index']
    ],
    'Form Koreksi Berkas',
];

$pemohon = $model;
$pengguna = $model->idPengguna;
$izin = $model->idJenisPerizinan;
$usaha = $model->idPerusahaan;

?> 


<div class="box-header with-border">
    <?= $this->title ?>

</div>

<div class="box-body">

    <div class="row">

        <div class="col-sm-9 col-md-offset-1 ">

            <div class="callout callout-info">

                <p>Informasi :</p>
                <ul>
                    <li>Harap diperiksa isian dari permohonan dibawah. Jika ada kesalahan silahkan isi bagian koreksi dibawah beserta komentar</li>
                </ul>

            </div>

            <div id="rootwizard" class="tabbable">
                <ul>
                    <li><a href="#tab1" data-toggle="tab">Pemohon</a></li>
                    <li><a href="#tab2" data-toggle="tab">Perusahaan</a></li>
                    <li><a href="#tab3" data-toggle="tab">Detail Permohonan</a></li>

                </ul>

                <form action="" method="post" class="form-horizontal" role="form">
                    


                <div class="tab-content">

                    <div class="tab-pane" id="tab1">

                       <div class="form-group" style="margin-top: 20px;">
                           <?= Html::activeLabel($pengguna,'nama',[
                               'class'=>'col-sm-2 control-label',

                           ]) ?>

                        <div class="col-sm-9">
                            <?= Html::activeInput('text',$pengguna,'nama',[
                                'class'=>'form-control',
                                'readonly'=>true,
                            ]); ?>
                        </div>
                       </div>

                        <div class="form-group"">
                            <?= Html::activeLabel($pengguna,'alamat',[
                                'class'=>'col-sm-2 control-label'
                            ]) ?>

                            <div class="col-sm-9">
                                <?= Html::activeTextarea($pengguna,'alamat',[
                                    'class'=>'form-control',
                                    'readonly'=>true,
                                ]); ?>
                            </div>
                        </div>

                        <div class="form-group" style="margin-top: 20px;">
                           <?= Html::activeLabel($izin,'nama_izin',[
                               'class'=>'col-sm-2 control-label'
                           ]) ?>

                            <div class="col-sm-9">
                                <?= Html::activeInput('text',$izin,'nama_izin',[
                                    'class'=>'form-control',
                                    'readonly'=>true,
                                ]); ?>
                            </div>
                       </div>

                    </div>
                    <div class="tab-pane" id="tab2">
                       <div class="form-group" style="margin-top: 20px;">
                           <?= Html::activeLabel($usaha,'nama_perusahaan',[
                               'class'=>'col-sm-2 control-label'
                           ]) ?>

                            <div class="col-sm-9">
                                <?= Html::activeInput('text',$usaha,'nama_perusahaan',[
                                    'class'=>'form-control',
                                    'readonly'=>true,
                                ]); ?>
                            </div>
                       </div>
                       <div class="form-group" style="margin-top: 20px;">
                           <?= Html::activeLabel($usaha,'nama_pemilik_pj',[
                               'class'=>'col-sm-2 control-label'
                           ]) ?>

                            <div class="col-sm-9">
                                <?= Html::activeInput('text',$usaha,'nama_pemilik_pj',[
                                    'class'=>'form-control',
                                    'readonly'=>true,
                                ]); ?>
                            </div>
                       </div>

                       <div class="form-group" style="margin-top: 20px;">
                           <?= Html::activeLabel($usaha,'npwp',[
                               'class'=>'col-sm-2 control-label'
                           ]) ?>

                            <div class="col-sm-9">
                                <?= Html::activeInput('text',$usaha,'npwp',[
                                    'class'=>'form-control',
                                    'readonly'=>true,
                                ]); ?>
                            </div>
                       </div>

                    </div>
                    <div class="tab-pane" id="tab3">
                        <?php
                         $dataIsian = \app\models\DataIsianPerizinan::find()
                                        ->where(['id_pemohon' => $pemohon->id_pemohon])
                                        ->all();

                        foreach ($dataIsian as $k=>$isi):
                         $kolom = $isi->idDataKolom;
                         ?>

                                <div class="form-group" style="margin-top: 20px;">
                           <?= Html::label($kolom->nama_kolom,[],[
                               'class'=>'col-sm-2 control-label'
                           ]) ?>

                            <div class="col-sm-9">
                                <?= Html::activeInput('text',$isi,'isian',[
                                    'class'=>'form-control',
                                    'readonly'=>true,
                                ]); ?>
                            </div>
                       </div>

                         <?php endforeach; ?>
                    </div>

                    <ul class="pager wizard">
                        <li class="previous first" style="display:none;"><a href="#">First</a></li>
                        <li class="previous"><a href="#">Previous</a></li>
<!--                        <li class="next last" style="display:none;"><a href="#">Last</a></li>-->
                        <li class="next"><a href="#">Next</a></li>
                        <li class="next finish" style="display:none;"><a href="javascript:;">Finish</a></li>
                    </ul>

                </div>


                </form>

            </div>

        </div>
    </div>

    <div class="row">

        <div class="col-sm-9 col-md-offset-1 ">

            <div class="box box-info">

                <div class="box-header with-border">
                    <div class="box-title">LEMBAR KOREKSI</div>
                </div>

                <div class="box-body">
                    <form action="" method="post" class="form-horizontal" role="form">

                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                    
                    <div class="form-group">

                        <div class="col-md-12">
                            <textarea name="pesan_koreksi" style="height: 200px" class="form-control" id="" placeholder=""></textarea>
                        </div>
                    </div>
                    
                        
                    
                        <div class="form-group">
                        <div class="col-md-12">
                            <div class="pull-right">
                                <button type="submit" name="koreksi" class="btn btn-danger">Koreksi</button>
                                <button type="submit" name="next" class="btn btn-success">Lanjutkan untuk di Verifikasi</button>
                            </div>
                        </div>
                        </div>
                    </form>
                </div>


            </div>


        </div>
    </div>

</div>

<?php

$script = <<< JS
    $('#rootwizard').bootstrapWizard({
        'tabClass': 'nav nav-tabs',
        'onTabShow' : function(tab, navigation, index){
            var total = navigation.find('li').length;
            var current = index+1;
            var percent = (current/total) * 100;
            $('#rootwizard').find('.bar').css({width:percent+'%'});

            // If it's the last tab then hide the last button and show the finish instead
            if(current >= total) {
                $('#rootwizard').find('.pager .next').hide();
                $('#rootwizard').find('.pager .finish').show();
                $('#rootwizard').find('.pager .finish').removeClass('disabled');
            } else {
                $('#rootwizard').find('.pager .next').show();
                $('#rootwizard').find('.pager .finish').hide();
            }
        }
     });

     $('#rootwizard .finish').click(function() {
		alert('Finished!, Starting over!');
		$('#rootwizard').find("a[href*='tab1']").trigger('click');
	});
JS;

$this->registerJs($script);

list(,$url)=Yii::$app->assetManager->publish('@vendor/bower/twitter-bootstrap-wizard');
$this->registerJsFile($url.'/jquery.bootstrap.wizard.js',['depends'=>[\app\assets\AppAsset::className()]]);




