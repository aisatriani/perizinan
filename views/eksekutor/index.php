<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 23/02/16
 * Time: 15:30
 */

\app\assets\DataAsset::register($this);
$this->title = "Daftar Pengajuan permohonan";

?>


    <div class="box-header with-border">
        <div class="box-title">
            <?= $this->title ?> yang belum diterbitkan
        </div>

    </div>
    <div class="box-body">

        <?php if(count($model) == 0){  ?>


            <div class="callout callout-info">
                <p>Tidak ada data yang menunggu. Semua data sudah tereksekusi</p>
            </div>

        <?php }else{ ?>

        <table id="example2" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>No</th>
                <th>Nama Perusahaan</th>
                <th>Jenis Perizinan</th>
                <th>Tanggal</th>
                <th>Status permohonan</th>
                <th>Opsi</th>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; foreach($model as $m): ?>
            <tr>
                <td><?= $no++ ?></td>
                <td><?= $m->idPerusahaan->nama_perusahaan ?></td>
                <td><?= $m->idJenisPerizinan->nama_izin ?></td>
                <td><?php
                    $datetime = date_create($m->tgl_permohonan) ;

                    echo date_format($datetime,"d-m-Y");
                    ?></td>
                <td><?php
                    $time = strtotime($m->tgl_terbit);
                    if($time){
                        echo 'Selesai';
                    }else{
                        echo 'Dalam Proses';
                    }
                    ?></td>
                <td>
                    <a class="btn btn-sm btn-info" href="<?= \yii\helpers\Url::to(['/permohonan/view/', 'id' => $m->id_pemohon]) ?>"><i class="fa fa-eye"></i> Lihat</a>
                    <?php if(Yii::$app->user->can('admin') || Yii::$app->user->can('eksekutor')): ?>
                    <?= \yii\bootstrap\Html::a(
                        ' Terbitkan',
                        ['eksekutor/terbit','id' => $m->id_pemohon],
                        [
                            'data-confirm'=>'Data ini telah disetujui dan akan diterbitkan segera ??',
                            'method'=>'post',
                            'class'=> 'fa fa-check-square-o btn btn-sm btn-warning']
                    ); ?>
                    <?php endif; ?>

                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <?php } ?>

    </div>
