<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 23/02/16
 * Time: 21:26
 */
use app\components\ArrayData;
use app\models\JenisPerizinan;
use app\models\Pengguna;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;


/* @var $form ActiveForm */
/* @var $verisian \app\models\VerifikasiIsian */
/* @var $validasi \app\models\Verifikasi */

$this->title = 'Form Validasi Berkas';
$this->params['breadcrumbs'] = [
    [
        'label' => 'validator',
        'url' => ['validator/index']
    ],
    'Form Validasi',
];




?>


    <div class="box-header with-border">
        <?= $this->title ?>

    </div>

    <div class="box-body">

<!--            <form class="form-horizontal">-->
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal'
                ]); ?>

        <?php if($verisian->hasErrors()): ?>

            <?= $form->errorSummary($verisian); ?>

        <?php endif; ?>

        <!--PESAN KOREKSI-->
        <?php if(!empty((array)$pemohon->pemohonDetail->pemohonKoreksi) && $pemohon->pemohonDetail->pemohonKoreksi->is_koreksi == 1): ?>
        <div class="callout callout-warning">
            <p>PESAN KOREKSI :</p>
            <ol>
                <span><?= $pemohon->pemohonDetail->pemohonKoreksi->koreksi ?></span>
            </ol>
        </div>
        <?php endif; ?>

        <!--PESAN KOREKSI-->
        <?php if(!empty((array)$pemohon->pemohonDetail->pemohonVerifikasi) && $pemohon->pemohonDetail->pemohonVerifikasi->is_verifikasi == 1): ?>
            <div class="callout callout-warning">
                <p>PESAN VERIFIKASI :</p>
                <ol>
                    <span><?= $pemohon->pemohonDetail->pemohonVerifikasi->verifikasi ?></span>
                </ol>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-sm-9 col-md-offset-1 ">
                <div class="box box-default collapsed-box">
                    <div class="box-header with-border">
                        <div class="box-title">DATA PEMOHON</div>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">

                        <?= $form->field($pemohon, 'nama_perizinan')->textInput([
                            'value'=>$pemohon->idJenisPerizinan->nama_izin,
                            'readonly' => true,
                        ]); ?>

                        <?= $form->field($pemohon, 'nama_pemohon')->textInput(['value' => $pemohon->idPengguna->nama, 'readonly' => 'true', 'class' => 'form-control spasi']); ?>
                        <?= $form->field($pemohon, 'alamat')->textInput(['value' => $pemohon->idPengguna->alamat, 'readonly' => 'true']); ?>
                        <?= $form->field($pemohon, 'nama_perusahaan')->textInput(['value' => $pemohon->idPerusahaan->nama_perusahaan, 'readonly' => 'true']); ?>

                    </div>

                </div>
            </div>
        </div>




        <div class="row" id="block-detail">
            <div class="col-sm-9 col-md-offset-1 ">
                <div class="box box-default">
                    <div class="box-header with-border">DATA DETAIL PERIZINAN</div>
                    <div class="box-body">



                        <?php
                            $checkIsi = \app\models\VerifikasiIsian::find()->where(['id_pemohon'=> $pemohon->id_pemohon])->all();
                            if($checkIsi){ ?>

                                <?php foreach($validasi as $i=>$k): ?>

                                    <?php

                                    foreach ($k->verifikasiIsians as $visi) {
                                        if($visi->id_pemohon == $pemohon->id_pemohon){ ?>


                                            <div class="form-group field-dataisianperizinan-isian required">
                                                <label class="control-label col-sm-3" for="DataIsianPerizinan-isian"><?= $k->nama_verifikasi ?></label>
                                                <div class="col-sm-6">
                                                    <?= Html::activeCheckbox($visi, "[$i]isi_verifikasi",['label'=>'']) ?>
                                                    <?= Html::activeHiddenInput($visi, "[$i]id_verifikasi",['class' => 'form-control spasi','value'=> $k->id_verifikasi]) ?>
                                                    <div class="help-block help-block-error "></div>
                                                </div>

                                            </div>

                                        <?php }
                                    }

                                    ?>

                                <?php endforeach; ?>

                        <?php }else{ ?>

                                <?php foreach($validasi as $i=>$k): ?>




                                            <div class="form-group field-dataisianperizinan-isian required">
                                                <label class="control-label col-sm-3" for="DataIsianPerizinan-isian"><?= $k->nama_verifikasi ?></label>
                                                <div class="col-sm-6">
                                                    <?= Html::activeCheckbox($verisian, "[$i]isi_verifikasi",['label'=>'']) ?>
                                                    <?= Html::activeHiddenInput($verisian, "[$i]id_verifikasi",['class' => 'form-control spasi','value'=> $k->id_verifikasi]) ?>
                                                    <div class="help-block help-block-error "></div>
                                                </div>

                                            </div>



                                <?php endforeach; ?>


                        <?php } ?>



                    </div>

                </div>


                <div class="callout callout-info">
                    <p>catatan :</p>
                    <ol>
                        <li>Harap periksa dengan teliti berkas yang akan divalidasi sesuai dengan berkas yang ada</li>
                        <li>Pengurusan izin akan diproses jika semua syarat terpenuhi. jika belum, simpan pengerjaan dan harap beritahukan untuk dilengkapi</li>
                    </ol>
                </div>

            </div>

        </div>







                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-md-4 col-md-offset-4">
                        <button type="reset" class="btn btn-default btn80" id="reset">Batal</button>
                        <?= Html::submitButton('Simpan', ['class' => 'btn btn-primary btn80']) ?>
                    </div>

                </div>
                <!-- /.box-footer -->
        <?php ActiveForm::end(); ?>



    </div>

<?php

$script = <<< JS


    $('#perusahaan-id_provinsi').on('change',function(){
       var id_prov = this.value;
       var select_kabupaten = $("#perusahaan-id_kabupaten");
       $.get('/alamat/kabupaten?id_prov='+id_prov, function(data){
           select_kabupaten.html('');
           select_kabupaten.append(new Option('Pilih Kabupaten/Kota',''));
           $.each(data, function(i, item){

               select_kabupaten.append(new Option(item.nama_kab, item.id_kab));

           });
       })
   });

   $('#perusahaan-id_kabupaten').on('click',function(){
        var id_kab = this.value;
        var select_kecamatan = $('#perusahaan-id_kecamatan');
       $.get('/alamat/kecamatan?id_kab='+id_kab, function(data){
           select_kecamatan.html('');
           select_kecamatan.append(new Option('Pilih Kecamatan',''));
           $.each(data, function(i, item){

               select_kecamatan.append(new Option(item.nama_kec, item.id_kec));

           });
       });
   });

    $('#perusahaan-id_kecamatan').on('click',function(){
        var id_kec = this.value;
        var select_kelurahan = $('#perusahaan-kelurahan');
        $.get('/alamat/kelurahan?id_kec='+id_kec, function(data){
            select_kelurahan.html('');
            select_kelurahan.append(new Option('Pilih Kelurahan',''));
            $.each(data, function(i, item){

                select_kelurahan.append(new Option(item.nama_kel, item.id_kel));

            });
        });
    });

    $('#pemohon-id_jenis_perizinan').on('change', function(){
        //alert('data');
        $("#block-perusahaan").removeClass('hidden');
        $("#block-detail").removeClass('hidden');
    });

    $('#reset').on('click',function(){
        $("#block-perusahaan").addClass('hidden');
        $("#block-detail").addClass('hidden');
    });
JS;

$this->registerJs($script);



