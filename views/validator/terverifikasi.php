<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 23/02/16
 * Time: 15:30
 */

\app\assets\DataAsset::register($this);
$this->title = "Daftar Berkas yang di verifikasi";

?>


    <div class="box-header with-border">
        <div class="box-title">
            <?= $this->title ?>
        </div>

    </div>
    <div class="box-body">

        <?php if(count($model) == 0){  ?>


            <div class="callout callout-info">
                <p>Tidak ada data permohonan yang terkoreksi</p>
            </div>

        <?php }else{ ?>

        <table id="example2" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>No</th>
                <th>Nama Perusahaan</th>
                <th>Jenis Perizinan</th>
                <th>Tanggal Pengajuan</th>
                <th>Status permohonan</th>
                <th>Opsi</th>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; foreach($model as $m): ?>
            <tr>
                <td><?= $no++ ?></td>
                <td><?= $m->pemohon0->idPerusahaan->nama_perusahaan ?></td>
                <td><?= $m->pemohon0->idJenisPerizinan->nama_izin ?></td>
                <td><?php
                    $datetime = date_create($m->pemohon0->tgl_permohonan) ;

                    echo date_format($datetime,"d-m-Y");
                    ?></td>
                <td><?php
                    $time = strtotime($m->pemohon0->tgl_terbit);
                    if($time){
                        echo 'Selesai';
                    }else{
                        echo 'Dalam Proses';
                    }
                    ?></td>
                <td>
                    <a class="btn btn-sm btn-info" href="<?= \yii\helpers\Url::to(['/permohonan/view/', 'id' => $m->pemohon]) ?>"><i class="fa fa-eye"></i> Detail</a>
                    <?php if(Yii::$app->user->can('admin')): ?>
                    <?= \yii\bootstrap\Html::a(
                        ' Validasi',
                        ['validator/berkas','id' => $m->pemohon],
                        [
                            //'data-confirm'=>'Yakin ingin menghapus data ini??',
                            'method'=>'post',
                            'class'=> 'fa fa-check-square-o btn btn-sm btn-warning']
                    ); ?>
                    <?php endif; ?>

                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <?php } ?>

    </div>
