/**
 * Created by aisatriani on 24/02/16.
 */

yii.allowAction = function ($e) {
    var message = $e.data('confirm');
    return message === undefined || yii.confirm(message, $e);
};
yii.confirm = function (message, ok, cancel) {

    bootbox.confirm(
        {
            message: message,
            buttons: {
                confirm: {
                    label: "OK"
                },
                cancel: {
                    label: "Cancel"
                }
            },
            callback: function (confirmed) {
                if (confirmed) {
                    !ok || ok();
                } else {
                    !cancel || cancel();
                }
            }
        }
    );
    // confirm will always return false on the first call
    // to cancel click handler
    return false;
}

$(document).ready(function(){

   $('#pengguna-id_provinsi').on('change',function(){
       var id_prov = this.value;
       var select_kabupaten = $("#pengguna-id_kabupaten");
       $.get('/alamat/kabupaten?id_prov='+id_prov, function(data){
           select_kabupaten.html('');
           select_kabupaten.append(new Option('Pilih Kabupaten/Kota',''));
           $.each(data, function(i, item){

               select_kabupaten.append(new Option(item.nama_kab, item.id_kab));

           });
       })
   });

   $('#pengguna-id_kabupaten').on('click',function(){
        var id_kab = this.value;
        var select_kecamatan = $('#pengguna-id_kecamatan');
       $.get('/alamat/kecamatan?id_kab='+id_kab, function(data){
           select_kecamatan.html('');
           select_kecamatan.append(new Option('Pilih Kecamatan',''));
           $.each(data, function(i, item){

               select_kecamatan.append(new Option(item.nama_kec, item.id_kec));

           });
       });
   });

    $('#pengguna-id_kecamatan').on('click',function(){
        var id_kec = this.value;
        var select_kelurahan = $('#pengguna-id_kelurahan');
        $.get('/alamat/kelurahan?id_kec='+id_kec, function(data){
            select_kelurahan.html('');
            select_kelurahan.append(new Option('Pilih Kelurahan',''));
            $.each(data, function(i, item){

                select_kelurahan.append(new Option(item.nama_kel, item.id_kel));

            });
        });
    });

});