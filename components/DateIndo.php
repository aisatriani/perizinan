<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 22/03/16
 * Time: 12:18
 */

namespace app\components;


class DateIndo
{

    const FORMAT_BULAN_TEXT = 1;
    const FORMAT_BULAN_ANGKA = 2;

    private static $months = [
        '1' => 'Januari',
        '2' => 'Februari',
        '3' => 'Maret',
        '4' => 'April',
        '5' => 'Mei',
        '6' => 'Juni',
        '7' => 'Juli',
        '8' => 'Agustus',
        '9' => 'September',
        '10' => 'Oktober',
        '11' => 'November',
        '12' => 'Desember',

    ];

    public static function createDate($format = 2)
    {
        if($format == static::FORMAT_BULAN_ANGKA){
            return date('d m Y');
        }elseif($format == static::FORMAT_BULAN_TEXT){
            return static::getFormatBulanText();
        }else{
            return date('d m Y');
        }
    }

    private static function getFormatBulanText()
    {
        $day = date('d');
        $year = date('Y');
        $month = date('m');
        foreach(static::$months as $k=>$v){
            if($k == $month){
                $res = $day . ' '.$v. ' '.$year;
                return $res;
            }
        }

        return 'undefined';
    }

}