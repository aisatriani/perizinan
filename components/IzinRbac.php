<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 28/02/16
 * Time: 21:18
 */

namespace app\components;


class IzinRbac
{

    public static function checkUserAccess()
    {
        $user = \Yii::$app->user;
        $auth = \Yii::$app->authManager;

        $roles = $auth->getRolesByUser($user->getId());
        foreach($roles as $role){

            if($user->can($role->name)){
                return true;
            }
        }

        return false;

    }

}