<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 25/02/16
 * Time: 0:04
 */

namespace app\components;


use yii\helpers\Json;
use yii\web\JsonResponseFormatter;

class PrettyJsonResponseFormatter extends JsonResponseFormatter
{
    protected function formatJson($response)
    {
        $response->getHeaders()->set('Content-Type', 'application/json; charset=UTF-8');
        if ($response->data !== null) {
            $response->content = Json::encode($response->data, JSON_PRETTY_PRINT);
        }
    }
}