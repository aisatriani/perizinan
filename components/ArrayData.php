<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 08/03/16
 * Time: 10:47
 */

namespace app\components;


class ArrayData
{

    const BP_YAYASAN = 1;
    const BP_PERORANGAN = 2;
    const BP_PER_KOMANDITER = 3;
    const BP_PERSEROAN_TERBATAS = 4;
    const BP_KOPERASI = 5;
    const BP_INSTANSI_PEMERINTAH = 6;
    const BP_PER_FIRMA = 7;
    const BP_BENTUK_LAIN = 8;

    public static function bentukPerusahaan()
    {
        $data = [
            ArrayData::BP_YAYASAN =>'Yayasan',
            ArrayData::BP_PERORANGAN =>'Perusahaan Perorangan',
            ArrayData::BP_PER_KOMANDITER =>'Persekutuan Komanditer',
            ArrayData::BP_PERSEROAN_TERBATAS =>'Perseroan Terbatas',
            ArrayData::BP_KOPERASI =>'Koperasi',
            ArrayData::BP_INSTANSI_PEMERINTAH =>'Instansi Pemerintah',
            ArrayData::BP_PER_FIRMA =>'Persekutuan Firma',
            ArrayData::BP_BENTUK_LAIN =>'Bentuk usaha lain',
        ];

        return $data;
    }

}