<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 23/02/16
 * Time: 16:11
 */

namespace app\assets;


use dmstr\web\AdminLteAsset;
use yii\web\AssetBundle;

class DataAsset extends AssetBundle
{
    public $sourcePath = '@vendor/almasaeed2010/adminlte';
    public $css = ['plugins/datatables/dataTables.bootstrap.css'];
    public $js = [
        'plugins/datatables/jquery.dataTables.min.js',
        'plugins/datatables/dataTables.bootstrap.min.js',
        'plugins/slimScroll/jquery.slimscroll.min.js',
        'plugins/fastclick/fastclick.js'
    ];
    public $depends = ['dmstr\web\AdminLteAsset'];
}