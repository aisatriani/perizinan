<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 24/02/16
 * Time: 23:46
 */

namespace app\controllers;


use app\models\Kabupaten;
use app\models\Kecamatan;
use app\models\Kelurahan;
use app\models\Provinsi;
use yii\web\Controller;
use yii\web\Response;

class AlamatController extends Controller
{
    public function actionProvinsi()
    {
        $model = Provinsi::find()->all();
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $model;
    }

    public function actionKabupaten($id_prov)
    {
        $model = Kabupaten::find()->where(['id_prov' => $id_prov])->all();
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $model;
    }

    public function actionKecamatan($id_kab)
    {
        $model = Kecamatan::find()->where(['id_kab' => $id_kab])->all();
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $model;
    }

    public function actionKelurahan($id_kec)
    {
        $model = Kelurahan::find()->where(['id_kec' => $id_kec])->all();
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $model;
    }
}