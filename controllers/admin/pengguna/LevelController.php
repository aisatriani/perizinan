<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 25/02/16
 * Time: 17:36
 */

namespace app\controllers\admin\pengguna;

use app\controllers\admin\PenggunaController;
use app\models\LevelPengguna;
use app\models\Pengguna;

class LevelController extends PenggunaController
{

    public function actionView()
    {
        $model = LevelPengguna::find()->all();
        return $this->render('view',[
            'model' => $model
        ]);
    }

    public function actionCreate()
    {
        $this->view->title = 'Form Tambah Tingkatan Pengguna';
        $model = new LevelPengguna();

        if ($model->load(\Yii::$app->request->post())) {
            //$model->attributes = \Yii::$app->request->post('Pengguna');
            if ($model->validate()) {
                // form inputs are valid, do something here
                if($model->save(false)){
                    \Yii::$app->getSession()->setFlash('success','data berhasil di perbaharui');
                    return $this->redirect(['admin/pengguna/level/view']);
                }else{
                    //var_dump($model->getErrors());
                    //return;
                }
            }else{


                //\Yii::$app->getSession()->setFlash('error','Terjadi masalah, harap periksa kembali inputan anda');
                //return $this->redirect(['admin/pengguna/create']);
            }
        }

        return $this->render('form', [
            'model' => $model,
        ]);
    }

    public function actionEdit($pid){
        $this->view->title = 'Form Ubah Tingkatan pengguna';
        $model = LevelPengguna::findOne($pid);

        if ($model->load(\Yii::$app->request->post())) {
            if ($model->validate()) {

                $model->update();

                \Yii::$app->getSession()->setFlash('success','data berhasil di perbaharui');
                return $this->redirect(['admin/pengguna/level/view']);
            }else{
                \Yii::$app->getSession()->setFlash('error','Terjadi kesalahan saat penginputan data');
                //return $this->goBack();
            }
        }



        return $this->render('form', [
            'model' => $model,
        ]);
    }

    public function actionDelete($pid)
    {
        $model = LevelPengguna::findOne($pid);
        $model->delete();

        \Yii::$app->getSession()->setFlash('success','data berhasil di hapus');
        return $this->redirect(['admin/pengguna/level/view']);
    }
}