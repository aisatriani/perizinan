<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 25/02/16
 * Time: 19:27
 */

namespace app\controllers\admin\perizinan;


use app\controllers\admin\PerizinanController;
use app\models\DataKolomPerizinan;

class DetilController extends PerizinanController
{

    public function actionView()
    {
        $model = DataKolomPerizinan::find()->orderBy('id_perizinan ASC')->all();
        return $this->render('view',['model' => $model]);
    }

    public function actionDelete($pid)
    {
        $model = DataKolomPerizinan::findOne($pid);
        $model->delete();

        \Yii::$app->getSession()->setFlash('success','data berhasil di hapus');
        return $this->redirect(['admin/perizinan/detil/view']);
    }

    public function actionCreate()
    {
        $this->view->title = 'Form Tambah perizinan';
        $model = new DataKolomPerizinan();

        if ($model->load(\Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here

                $model->save();
                \Yii::$app->getSession()->setFlash('success','data berhasil di perbaharui');
                return $this->redirect(['admin/perizinan/detil/view']);
            }else{
                \Yii::$app->getSession()->setFlash('error','Terjadi kesalahan saat penginputan data');
            }
        }

        return $this->render('form', [
            'model' => $model,
        ]);
    }

    public function actionEdit($pid){

        $this->view->title = 'Form Ubah Detil Perizinan';
        $model = DataKolomPerizinan::findOne($pid);

        if ($model->load(\Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                $model->update();
                \Yii::$app->getSession()->setFlash('success','data berhasil di perbaharui');
                return $this->redirect(['admin/perizinan/detil/view']);
            }else{
                \Yii::$app->getSession()->setFlash('error','Terjadi kesalahan saat penginputan data');
                return $this->goBack();
            }
        }

        return $this->render('form', [
            'model' => $model,
        ]);
    }

}