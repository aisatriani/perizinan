<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 26/02/16
 * Time: 9:21
 */

namespace app\controllers\admin;


use app\controllers\AdminController;
use app\models\Verifikasi;

class VerifikasiController extends AdminController
{
    public function actionView()
    {
        $model = Verifikasi::find()->all();
        return $this->render(
            'view', ['model' => $model]
        );
    }

    public function actionCreate()
    {
        $this->view->title = 'Form Detil Verifikasi';
        $model = new Verifikasi();

        if ($model->load(\Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here

                $model->save();
                \Yii::$app->getSession()->setFlash('success','data berhasil di perbaharui');
                return $this->redirect(['admin/verifikasi/view']);
            }else{
                \Yii::$app->getSession()->setFlash('error','Terjadi kesalahan saat penginputan data');
            }
        }

        return $this->render('form', [
            'model' => $model,
        ]);
    }

    public function actionEdit($pid){

        $this->view->title = 'Form Detil Verifikasi';
        $model = Verifikasi::findOne($pid);

        if ($model->load(\Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                $model->update();
                \Yii::$app->getSession()->setFlash('success','data berhasil di perbaharui');
                return $this->redirect(['admin/verifikasi/view']);
            }else{
                \Yii::$app->getSession()->setFlash('error','Terjadi kesalahan saat penginputan data');
            }
        }

        return $this->render('form', [
            'model' => $model,
        ]);
    }

    public function actionDelete($pid)
    {
        $model = Verifikasi::findOne($pid);
        $model->delete();

        \Yii::$app->getSession()->setFlash('success','data berhasil di hapus');
        return $this->redirect(['admin/verifikasi/view']);
    }
}