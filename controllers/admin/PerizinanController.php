<?php

/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 23/02/16
 * Time: 16:55
 */

namespace app\controllers\admin;

use app\controllers\AdminController;
use app\models\JenisPerizinan;
use yii\web\Controller;

class PerizinanController extends AdminController
{

    public function actionView(){
        $model = JenisPerizinan::find()->orderBy('id_izin DESC')->all();
        return $this->render('izin',['model' => $model]);
    }

    public function actionCreate()
    {
        $this->view->title = 'Form Tambah perizinan';
        $model = new JenisPerizinan();

        if ($model->load(\Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                $model->created_at = time();
                $model->last_id = 1;
                $model->save();
                \Yii::$app->getSession()->setFlash('success','data berhasil di perbaharui');
                return $this->redirect(['admin/perizinan/view']);
            }else{
                \Yii::$app->getSession()->setFlash('error','Terjadi kesalahan saat penginputan data');
                return $this->redirect(['admin/perizinan/create']);
            }
        }

        return $this->render('form', [
            'model' => $model,
        ]);
    }



    public function actionEdit($pid){
        $this->view->title = 'Form Ubah perizinan';
        $model = JenisPerizinan::findOne($pid);

        if ($model->load(\Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                //$model->id_izin = $pid;
                $model->update();
                //$model->save();
                \Yii::$app->getSession()->setFlash('success','data berhasil di perbaharui');
                return $this->redirect(['admin/perizinan/view']);
            }else{
                \Yii::$app->getSession()->setFlash('error','Terjadi kesalahan saat penginputan data');
                return $this->goBack();
            }
        }



        return $this->render('form', [
            'model' => $model,
        ]);
    }

    public function actionDelete($pid)
    {
        $model = JenisPerizinan::findOne($pid);
        $model->delete();

        \Yii::$app->getSession()->setFlash('success','data berhasil di hapus');
        return $this->redirect(['admin/perizinan/view']);
    }

}