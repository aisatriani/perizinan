<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 24/02/16
 * Time: 21:31
 */

namespace app\controllers\admin;


use app\controllers\AdminController;
use app\models\Pengguna;


class PenggunaController extends AdminController
{



    public function actionView()
    {
        $model = Pengguna::find()->all();
        return $this->render('view',[
           'model' => $model
        ]);
    }

    public function actionCreate()
    {
        $this->view->title = 'Form Tambah Pengguna';
        $model = new Pengguna();

        if ($model->load(\Yii::$app->request->post())) {
            //$model->attributes = \Yii::$app->request->post('Pengguna');
            if ($model->validate()) {
                // form inputs are valid, do something here
                $model->generatePassword($model->password);
                if($model->save(false)){
                    \Yii::$app->getSession()->setFlash('success','data berhasil di perbaharui');
                    return $this->redirect(['admin/pengguna/view']);
                }else{

                }
            }
        }

        return $this->render('form', [
            'model' => $model,
        ]);
    }

    public function actionEdit($pid){
        $this->view->title = 'Form Ubah pengguna';
        $model = Pengguna::findOne($pid);
        $model->id_kecamatan = $model->idKelurahan->id_kec;
        $model->id_kabupaten = $model->idKelurahan->idKec->id_kab;
        $model->id_provinsi = $model->idKelurahan->idKec->idKab->id_prov;

        if ($model->load(\Yii::$app->request->post())) {
            if ($model->validate()) {

                $model->update();

                \Yii::$app->getSession()->setFlash('success','data berhasil di perbaharui');
                return $this->redirect(['admin/pengguna/view']);
            }else{

            }
        }



        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($pid)
    {
        $model = Pengguna::findOne($pid);
        $model->delete();

        \Yii::$app->getSession()->setFlash('success','data berhasil di hapus');
        return $this->redirect(['admin/pengguna/view']);
    }

}