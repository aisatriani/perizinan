<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 14/04/16
 * Time: 21:49
 */

namespace app\controllers;


use app\models\Pemohon;
use app\models\PemohonDetail;
use yii\web\Controller;

class EksekutorController extends Controller
{

    public function actionIndex()
    {
        return $this->render('index',[
            'model' => Pemohon::find()
                ->joinWith('pemohonDetail')
                ->where(['pemohon_detail.koreksi' => 1])
                ->andWhere(['pemohon_detail.validasi' => 1])
                ->andWhere(['pemohon_detail.verifikasi' => 1])
                ->andWhere(['pemohon_detail.eksekusi' => 0])
                ->all()
        ]);
    }

    public function actionTerbit($id)
    {

            $pemohon = Pemohon::findOne($id);
            $pemohon->tgl_terbit = date("Y-m-d H:i:s");
            $pemohon->save(false);

            $detail = PemohonDetail::findOne($id);
            $detail->eksekusi = 1;
            $detail->no_terbit = $this->generateNoTerbit($pemohon);
            $detail->tgl_eksekusi = date('Y-m-d H:i:s');
            $detail->save(false);

            \Yii::$app->getSession()->setFlash('success','Permohonan berhasil di terbitkan');
            return $this->redirect(['/eksekutor/index']);

    }

    public function generateNoTerbit($pemohon)
    {

        $nop = str_pad($pemohon->id_pemohon, 4, '0', STR_PAD_LEFT);
        $upizin = strtoupper( $pemohon->idJenisPerizinan->singkatan );
        $noi = str_pad($pemohon->id_jenis_perizinan, 2, '0', STR_PAD_LEFT);
        $tahun = date("Y");

        return $nop.'/BPM-PTSP/SK/'.$upizin.'/'.$noi.'/'.$tahun;

    }

}