<?php

namespace app\controllers;

use app\models\DataIsianPerizinan;
use app\models\DataKolomPerizinan;
use app\models\Perusahaan;
use kartik\mpdf\Pdf;
use Yii;
use app\models\Pemohon;
use app\models\PemohonSearch;
use yii\base\Model;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PermohonanController implements the CRUD actions for Pemohon model.
 */
class PermohonanController extends Controller
{
    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'only' => ['view','create','index','izin','izindetail','delete','status','list','cek'],
                'rules' => [
//                    [
//                        'actions' => ['view'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                        'matchCallback' => function ($rule, $action) {
//                            if (Yii::$app->user->can('admin') || $this->isOwn()) {
//                                return true;
//                            }
//                            return false;
//                        }
//                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (Yii::$app->user->can('admin') || $this->isOwn()) {
                                return true;
                            }
                            return false;
                        }
                    ],
                    [
                        'actions' => ['index','izin','izindetail','status','list','cek','view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],


        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Lists all Pemohon models.
     * @return mixed
     */
    public function actionIndex()
    {

        $this->redirect(['list']);

//        $searchModel = new PemohonSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
    }

    /**
     * Displays a single Pemohon model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'isian' => DataIsianPerizinan::find()->where(['id_pemohon'=> $id])->all(),
        ]);
    }

    public function actionList()
    {
        $model = Pemohon::find()->all();

        if(Yii::$app->request->isPost){
            $model = Pemohon::find()
                ->joinWith('pemohonDetail')
                ->where([
                    'like','id_jenis_perizinan',Yii::$app->request->post('nama_izin')
                ])
                ->andWhere(['like','pemohon_detail.eksekusi',Yii::$app->request->post('status')])
                ->all();

        }

        return $this->render('list',[
            'model' => $model
        ]);
    }

    public function actionStatus()
    {

        $model = Pemohon::find()->where(
            ['id_pengguna'=> Yii::$app->user->id])->all();

        return $this->render('status',[
           'model' => $model
        ]);

    }

    public function actionCek()
    {
        $model = new Pemohon();

        if(Yii::$app->request->isPost){
            $no = Yii::$app->request->post('no_permohonan');
            $model = Pemohon::find()
                ->joinWith('pemohonDetail')
                //->where(['like','pemohon_detail.no_pemohon',$no])
                ->where(['pemohon_detail.no_pemohon'=>$no])
                ->all();
        }

        return $this->render('cek_form',[
            'model'=> $model
        ]);
    }

    public function actionPreview($id)
    {
        $model = Pemohon::findOne($id);
        $content = $this->renderPartial('_kartuTandaTerima',['model'=>$model]);
        return $content;
    }

    public function actionCetakTanda($id)
    {
        // get your HTML raw content without any layouts or scripts
        $model = Pemohon::findOne($id);
        $content = $this->renderPartial('_kartuTandaTerima',['model'=>$model]);

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Tanda Penerimaan Berkas Pengurusan Izin'],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>['Krajee Report Header'],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    public function actionIzin()
    {
        $pemohon = new Pemohon();
        $perusahaan = new Perusahaan();
        $kolom = DataKolomPerizinan::find()->all();

        $isian = new DataIsianPerizinan();


        if ($perusahaan->load(Yii::$app->request->post()) && $perusahaan->save()) {
            if($pemohon->load(Yii::$app->request->post())){
                $pemohon->id_pengguna = Yii::$app->user->id;
                $pemohon->id_perusahaan = $perusahaan->id_perusahaan;
                if($pemohon->save()){
                    $isianArr = [new DataIsianPerizinan()];
                    $arrdata = count(Yii::$app->request->post('DataIsianPerizinan'));
                    for($i = 0; $i < $arrdata; $i++){
                        $isianArr[] = new DataIsianPerizinan();
                    }

                    if(Model::loadMultiple($isianArr,Yii::$app->request->post())){
                        foreach($isianArr as $isi){
                            $isi->id_pemohon = $pemohon->id_pemohon;
                            if($isi->save()){
                                echo 'save isian';
                            }else{
                                var_dump($isi->errors);
                            }

                        }
                    }else{
                        echo 'error save isian';
                    }


                    //var_dump($isianArr);
                    echo 'last success';

                }else{
                    var_dump($pemohon->getErrors());
                }

            }
        } else {
            return $this->render('request', [
                'pemohon' => $pemohon,
                'perusahaan' => $perusahaan,
                'kolom' => $kolom,
                'isian' => $isian,
            ]);
        }
    }

    public function actionIzindetail()
    {

        if(!Yii::$app->request->isPost){
            $this->redirect(['permohonan/izin']);
            return;
        }

        $post = Yii::$app->request->post('Pemohon');
        $idizin = $post['id_jenis_perizinan'];

        $pemohon = new Pemohon();
        $pemohon->id_jenis_perizinan = $idizin;
        $perusahaan = new Perusahaan();
        $kolom = DataKolomPerizinan::find()->where(['id_perizinan'=>$idizin])->all();

        $isian = new DataIsianPerizinan();


        if ($perusahaan->load(Yii::$app->request->post()) && $perusahaan->save()) {
            if($pemohon->load(Yii::$app->request->post())){
                $pemohon->id_pengguna = Yii::$app->user->id;
                $pemohon->id_perusahaan = $perusahaan->id_perusahaan;
                if($pemohon->save()){
                    $isianArr = [new DataIsianPerizinan()];
                    $arrdata = count(Yii::$app->request->post('DataIsianPerizinan'));
                    for($i = 0; $i < $arrdata; $i++){
                        $isianArr[] = new DataIsianPerizinan();
                    }

                    if(Model::loadMultiple($isianArr,Yii::$app->request->post())){
                        foreach($isianArr as $isi){
                            $isi->id_pemohon = $pemohon->id_pemohon;
                            if($isi->save()){
                                //echo 'save isian';
                            }else{
                                //var_dump($isi->errors);
                            }

                        }
                    }else{
                        //echo 'error save isian';
                    }


                    //var_dump($isianArr);
                    //echo 'last success';
                    return $this->redirect(['view', 'id' => $pemohon->id_pemohon]);


                }else{
                    var_dump($pemohon->getErrors());
                }

            }
        } else {
            return $this->render('request2', [
                'pemohon' => $pemohon,
                'perusahaan' => $perusahaan,
                'kolom' => $kolom,
                'isian' => $isian,
            ]);
        }
    }

    /**
     * Creates a new Pemohon model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pemohon();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_pemohon]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pemohon model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_pemohon]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pemohon model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['list']);
    }

    public function actionTerbit()
    {

        return $this->render('terbit',[
            'model' => Pemohon::find()
                ->joinWith('pemohonDetail')
                ->where(['pemohon_detail.koreksi' => 1])
                ->andWhere(['pemohon_detail.validasi' => 1])
                ->andWhere(['pemohon_detail.verifikasi' => 1])
                ->andWhere(['pemohon_detail.eksekusi' => 1])
                ->all()
        ]);

    }

    private function pdf($content)
    {

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_LEGAL,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Surat Izin'],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>['Krajee Report Header'],
                //'SetFooter'=>['{PAGENO}'],
                'SetFooter'=>['Pengurusan izin tidak dipungut biaya']
            ],
            'marginTop' => 10,
            'marginBottom' => 5,
        ]);

        return $pdf;

    }

    private function printAKDP($model)
    {
        $terminal_asal = $this->getValue($model, 7);
        $terminal_tujuan = $this->getValue($model, 8);
        $plat_no = $this->getValue($model, 9);
        $nama_pemilik = $this->getValue($model, 10);
        $alamat = $this->getValue($model, 11);
        $tahun_pembuatan = $this->getValue($model, 12);
        $no_uji = $this->getValue($model, 13);

        $tahun_terbit = date_create($model->tgl_terbit);
        $tahun_terbit = date_format($tahun_terbit, 'd-m-Y');
        $tahun_berakhir = date('d-m-Y', strtotime('+5 year', strtotime($tahun_terbit)));

        $content = $this->renderPartial('_surat_akdp',[
            'model'=>$model,
            'terminal_asal' => $terminal_asal,
            'terminal_tujuan' => $terminal_tujuan,
            'plat_no' => $plat_no,
            'nama_pemilik' => $nama_pemilik,
            'alamat' => $alamat,
            'tahun_pembuatan' => $tahun_pembuatan,
            'no_uji' => $no_uji,
            'tahun_terbit' => $tahun_terbit,
            'tahun_berakhir' => $tahun_berakhir

        ]);

        // return the pdf output as per the destination setting
        return $this->pdf($content)->render();

    }

    private function printSIUPBM($model)
    {

        $content = $this->renderPartial('_surat_siupbm',[
            'model' => $model
        ]);

        // return the pdf output as per the destination setting
        return $this->pdf($content)->render();

    }

    private function printSIUJPT($model)
    {
        $content = $this->renderPartial('_surat_siujpt',[
            'model' => $model
        ]);

        // return the pdf output as per the destination setting
        return $this->pdf($content)->render();
    }

    public function actionCetak($id)
    {
        // get your HTML raw content without any layouts or scripts
        $model = Pemohon::findOne($id);
        if($model->idJenisPerizinan->singkatan == 'akdp'){
            $this->printAKDP($model);
        }
        if($model->idJenisPerizinan->singkatan == 'siupbm'){
            $this->printSIUPBM($model);
        }
        if($model->idJenisPerizinan->singkatan == 'siujpt'){
            $this->printSIUJPT($model);
        }

    }

    private function getValue($model, $id_kolom){
        foreach($model->dataIsianPerizinans as $dataisi){
            if($dataisi->idDataKolom->id_kolom == $id_kolom){
                return $dataisi->isian;
            }
        }

        return '';
    }

    /**
     * Finds the Pemohon model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pemohon the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pemohon::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function isOwn()
    {
        return $this->findModel(Yii::$app->request->get('id'))->idPengguna->id_pengguna == Yii::$app->user->id;
    }
}
