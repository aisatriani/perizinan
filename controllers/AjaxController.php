<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 20/03/16
 * Time: 11:09
 */

namespace app\controllers;

use app\models\PemohonDetail;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;
use yii\web\Response;

class AjaxController extends Controller
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function actionNoPemohon()
    {
        $data = PemohonDetail::find()->select('no_pemohon')->all();
        $datatemp = [];
        foreach($data as $k=>$d){
            $datatemp[$k] = $d->no_pemohon;
        }
        return $datatemp;

    }

}