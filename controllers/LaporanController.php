<?php

namespace app\controllers;

use yii\web\NotFoundHttpException;

class LaporanController extends \yii\web\Controller
{
    public function actionIndex()
    {
        throw new NotFoundHttpException('Halaman tidak ditemukan');
    }

    public function actionCetakIzin()
    {

    }

}
