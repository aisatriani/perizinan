<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 23/02/16
 * Time: 12:34
 */

namespace app\controllers;


use app\models\JenisPerizinan;
use dmstr\web\AdminLteAsset;
use yii\filters\AccessControl;

class AdminController extends BaseController
{
//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => ['index','view'],
//                'rules' => [
//                    [
//                        'actions' => ['index'],
//                        'allow' => true,
//                        'roles' => ['?'],
//                    ],
//                    [
//                        'actions' => ['view'],
//                        'allow' => true,
//                        'roles' => ['?'],
//                    ],
//
//                ],
//            ],
//
//        ];
//    }


    public function actionIndex()
    {
        return $this->render('index');
    }

}