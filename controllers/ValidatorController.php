<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 12/03/16
 * Time: 8:48
 */

namespace app\controllers;


use app\models\Pemohon;
use app\models\PemohonDetail;
use app\models\Verifikasi;
use app\models\VerifikasiIsian;
use Yii;
use yii\base\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ValidatorController extends Controller
{

    public function actionIndex()
    {
        return $this->render('index',[
            'model' => Pemohon::find()
                ->joinWith('pemohonDetail')
                ->where(['pemohon_detail.validasi' => Pemohon::VALIDASI_NO])
                ->all()
        ]);
    }

    public function actionBerkas($id)
    {
        $pemohon = Pemohon::findOne($id);
        $verisian = new VerifikasiIsian();

        if($pemohon == null){
           throw new NotFoundHttpException('Halaman yang kamu minta tidak ada');
        }


        $validasi = Verifikasi::find()->where([
            'id_perizinan' => $pemohon->id_jenis_perizinan
        ])->all();

        $isianArr = [new VerifikasiIsian()];
        $arrdata = count(Yii::$app->request->post('VerifikasiIsian'));
        for($i = 0; $i < $arrdata; $i++){
            $isianArr[] = new VerifikasiIsian();
        }

        if(Yii::$app->request->isPost){
            if(Model::loadMultiple($isianArr,Yii::$app->request->post()) ){
                foreach ($isianArr as $k=>$isi) {

                    $isi->id_pemohon = $pemohon->id_pemohon;
                    $isi->id_jenis_perizinan = $pemohon->id_jenis_perizinan;

                    $check = VerifikasiIsian::findOne([
                        'id_verifikasi' => $isi->id_verifikasi,
                        'id_pemohon' => $isi->id_pemohon,
                    ]);



                    if(!$check){
                        //echo 'test';
                        if(!$isi->save()){
                            //var_dump($isi->errors);
                        }
                    }else{

                        $check->id_verifikasi = $isi->id_verifikasi;
                        $check->isi_verifikasi = $isi->isi_verifikasi;
                        $check->update();
                    }



                }

                if($this->updateValidasiPemohon($pemohon->id_pemohon)){
                    $pemohonDetail = $this->findPemohonDetail($pemohon->id_pemohon);
                    $pemohonDetail->validasi = 1;
                    $pemohonDetail->tgl_validasi = date("Y-m-d H:i:s");
                    $pemohonDetail->update();
                    //$pemohon->verifikasi = 1;
                    //$pemohon->update();
                }

                $this->redirect(['/validator']);


            }
        }



//        var_dump($isianArr);

        return $this->render('form',[
            'pemohon' => $pemohon,
            'validasi' => $validasi,
            'verisian' => $verisian,
        ]);
    }

    public function updateValidasiPemohon($id)
    {
        $q = VerifikasiIsian::find()->where(['id_pemohon'=>$id])->all();

        foreach ($q as $r) {

            if($r->isi_verifikasi == 0){
                //echo 'false';
                return false;
            }

        }

        //echo 'true';

        return true;
    }

    public function actionKoreksi()
    {
        return $this->render('terkoreksi',[
            'model' => PemohonDetail::find()
                ->joinWith('pemohonKoreksi')
                ->where(['pemohon_detail.validasi' => Pemohon::VALIDASI_NO])
                ->andWhere(['pemohon_koreksi.is_koreksi' => 1])
                ->all()
        ]);
    }

    public function actionVerifikasi()
    {
        return $this->render('terverifikasi',[
            'model' => PemohonDetail::find()
                ->joinWith('pemohonVerifikasi')
                ->where(['pemohon_detail.validasi' => Pemohon::VALIDASI_NO])
                ->andWhere(['pemohon_verifikasi.is_verifikasi' => 1])
                ->all()
        ]);
    }

    protected function findPemohonDetail($id)
    {
        if (($model = PemohonDetail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}