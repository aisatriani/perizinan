<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 23/03/16
 * Time: 14:03
 */

namespace app\controllers;


use app\models\Pemohon;
use app\models\PemohonDetail;
use app\models\PemohonKoreksi;
use yii\web\Controller;

class KoreksiController extends Controller
{

    public function actionIndex()
    {
        return $this->render('index',[
            'model' => Pemohon::find()
                ->joinWith('pemohonDetail')
                ->where(['pemohon_detail.koreksi' => 0])
                ->andWhere(['pemohon_detail.validasi' => 1])
                ->all()
        ]);
    }

    public function actionBerkas($id)
    {
        $model = Pemohon::findOne($id);

        if(\Yii::$app->request->isPost){

            if(isset($_POST['koreksi'])){
                //var_dump($_POST);

                $pemohon = PemohonDetail::findOne($id);
                $pemohon->validasi = 0;
                $pemohon->save(false);

                $koreksi = PemohonKoreksi::findOne($id);
                if(empty((array) $koreksi)){
                    $koreksi = new PemohonKoreksi();
                }
                $koreksi->id_pemohon = $id;
                $koreksi->koreksi = \Yii::$app->request->post('pesan_koreksi');
                $koreksi->is_koreksi = PemohonKoreksi::KOREKSI_YES;
                $koreksi->save(false);


                return $this->redirect(['/koreksi/index']);
            }

            if(isset($_POST['next'])){
                $pemohon = PemohonDetail::findOne($id);
                $pemohon->koreksi = 1;
                $pemohon->tgl_verifikasi = date('Y-m-d H:i:s');
                $pemohon->save(false);

                $koreksi = PemohonKoreksi::findOne($id);
                if(empty((array) $koreksi)){
                    $koreksi = new PemohonKoreksi();
                }
                $koreksi->id_pemohon = $id;
                $koreksi->is_koreksi = PemohonKoreksi::KOREKSI_NO;
                $koreksi->save(false);
                return $this->redirect(['/koreksi/index']);
            }
        }

        return $this->render('form',[
            'model'=>$model,
        ]);
    }
}