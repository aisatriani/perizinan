<?php
/**
 * Created by IntelliJ IDEA.
 * User: aisatriani
 * Date: 12/03/16
 * Time: 8:47
 */

namespace app\controllers;



use app\models\Pemohon;
use app\models\PemohonDetail;
use app\models\PemohonVerifikasi;
use yii\web\Controller;

class VerifikasiController extends Controller
{

    public function actionIndex()
    {
        return $this->render('index',[
            'model' => Pemohon::find()
                ->joinWith('pemohonDetail')
                ->where(['pemohon_detail.koreksi' => 1])
                ->andWhere(['pemohon_detail.validasi' => 1])
                ->andWhere(['pemohon_detail.verifikasi' => 0])
                ->all()
        ]);
    }


    public function actionBerkas($id)
    {
        $model = Pemohon::findOne($id);

        if(\Yii::$app->request->isPost){

            if(isset($_POST['verifikasi'])){
                //var_dump($_POST);

                $pemohon = PemohonDetail::findOne($id);
                $pemohon->validasi = 0;
                $pemohon->save(false);

                $verifikasi = PemohonVerifikasi::findOne($id);
                if(empty((array) $verifikasi)){
                    $verifikasi = new PemohonVerifikasi();
                }
                $verifikasi->id_pemohon = $id;
                $verifikasi->verifikasi = \Yii::$app->request->post('pesan_koreksi');
                $verifikasi->is_verifikasi = 1;
                $verifikasi->save(false);


                return $this->redirect(['/verifikasi/index']);
            }

            if(isset($_POST['next'])){
                $pemohon = PemohonDetail::findOne($id);
                $pemohon->verifikasi = 1;
                $pemohon->tgl_verifikasi = date('Y-m-d H:i:s');
                $pemohon->save(false);

                $verifikasi = PemohonVerifikasi::findOne($id);
                if(empty((array) $verifikasi)){
                    $verifikasi = new PemohonVerifikasi();
                }
                $verifikasi->id_pemohon = $id;
                $verifikasi->is_verifikasi = 0;
                $verifikasi->save(false);
                return $this->redirect(['/verifikasi/index']);
            }
        }

        return $this->render('form',[
            'model'=>$model,
        ]);
    }


}