<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kelurahan".
 *
 * @property string $id_kel
 * @property string $id_kec
 * @property string $nama_kel
 *
 * @property Kecamatan $idKec
 * @property Pengguna[] $penggunas
 */
class Kelurahan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kelurahan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_kel'], 'required'],
            [['id_kel'], 'string', 'max' => 10],
            [['id_kec'], 'string', 'max' => 6],
            [['nama_kel'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kel' => 'Id Kel',
            'id_kec' => 'Id Kec',
            'nama_kel' => 'Nama Kel',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdKec()
    {
        return $this->hasOne(Kecamatan::className(), ['id_kec' => 'id_kec']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenggunas()
    {
        return $this->hasMany(Pengguna::className(), ['id_kelurahan' => 'id_kel']);
    }
}
