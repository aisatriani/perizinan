<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pemohon;

/**
 * PemohonSearch represents the model behind the search form about `app\models\Pemohon`.
 */
class PemohonSearch extends Pemohon
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pemohon', 'id_pengguna', 'id_perusahaan', 'id_jenis_perizinan', 'verifikasi', 'created_at'], 'integer'],
            [['tgl_permohonan', 'tgl_terbit'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pemohon::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_pemohon' => $this->id_pemohon,
            'id_pengguna' => $this->id_pengguna,
            'id_perusahaan' => $this->id_perusahaan,
            'id_jenis_perizinan' => $this->id_jenis_perizinan,
            'tgl_permohonan' => $this->tgl_permohonan,
            'tgl_terbit' => $this->tgl_terbit,
            'verifikasi' => $this->verifikasi,
            'created_at' => $this->created_at,
        ]);

        return $dataProvider;
    }
}
