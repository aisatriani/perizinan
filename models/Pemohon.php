<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pemohon".
 *
 * @property integer $id_pemohon
 * @property integer $id_pengguna
 * @property integer $id_perusahaan
 * @property integer $id_jenis_perizinan
 * @property string $tgl_permohonan
 * @property string $tgl_terbit
 * @property integer $created_at
 *
 * @property DataIsianPerizinan[] $dataIsianPerizinans
 * @property Pengguna $idPengguna
 * @property JenisPerizinan $idJenisPerizinan
 * @property Perusahaan $idPerusahaan
 * @property VerifikasiIsian[] $verifikasiIsians
 */
class Pemohon extends \yii\db\ActiveRecord
{

    const VALIDASI_NO = 0;
    const VALIDASI_YES = 1;
    public $nama_pemohon;
    public $alamat;
    public $nama_perizinan;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pemohon';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pengguna', 'id_perusahaan', 'id_jenis_perizinan'], 'required'],
            [['id_pengguna', 'id_perusahaan', 'id_jenis_perizinan', 'created_at'], 'integer'],
            [['tgl_permohonan', 'tgl_terbit'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pemohon' => 'Id Pemohon',
            'id_pengguna' => 'Id Pengguna',
            'id_perusahaan' => 'Id Perusahaan',
            'id_jenis_perizinan' => 'Jenis Perizinan',
            'tgl_permohonan' => 'Tgl Permohonan',
            'tgl_terbit' => 'Tgl Terbit',
            'created_at' => 'Created At',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
        if($insert){



            $detail = new PemohonDetail();
            $detail->pemohon = $this->id_pemohon;
            $detail->no_pemohon = $this->generateNumber();
            $detail->validasi = 0;
            $detail->koreksi = 0;
            $detail->verifikasi = 0;
            $detail->eksekusi = 0;
            $detail->save();
        }
    }

    public function generateNumber()
    {

        $nop = str_pad($this->id_pemohon, 4, '0', STR_PAD_LEFT);
        $upizin = strtoupper( $this->idJenisPerizinan->singkatan );
        $noi = str_pad($this->id_jenis_perizinan, 2, '0', STR_PAD_LEFT);
        $tahun = date("Y");

        return 'TT/'.$nop.'/'.$upizin.'/'.$noi.'/'.$tahun;

    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                //$this->auth_key = Yii::$app->security->generateRandomString();
                $this->tgl_permohonan = date("Y-m-d H:i:s");
                $this->created_at = time();
            }else{

            }

            return true;
        }
        return false;
    }


    public function getPemohonDetail()
    {
        return $this->hasOne(PemohonDetail::className(), ['pemohon' => 'id_pemohon']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataIsianPerizinans()
    {
        return $this->hasMany(DataIsianPerizinan::className(), ['id_pemohon' => 'id_pemohon']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPengguna()
    {
        return $this->hasOne(Pengguna::className(), ['id_pengguna' => 'id_pengguna']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdJenisPerizinan()
    {
        return $this->hasOne(JenisPerizinan::className(), ['id_izin' => 'id_jenis_perizinan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPerusahaan()
    {
        return $this->hasOne(Perusahaan::className(), ['id_perusahaan' => 'id_perusahaan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVerifikasiIsians()
    {
        return $this->hasMany(VerifikasiIsian::className(), ['id_pemohon' => 'id_pemohon']);
    }


    public function getDescriptionText()
    {
        $texts = [];
        foreach($this->dataIsianPerizinans as $descr) {
            $texts[] = $descr->isian;
        }
        return implode("\n", $texts);
    }
}
