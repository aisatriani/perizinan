<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data_kolom_perizinan".
 *
 * @property integer $id_kolom
 * @property string $nama_kolom
 * @property string $keterangan
 * @property integer $id_perizinan
 *
 * @property DataIsianPerizinan[] $dataIsianPerizinans
 * @property JenisPerizinan $idPerizinan
 */
class DataKolomPerizinan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_kolom_perizinan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_kolom', 'id_perizinan'], 'required'],
            [['id_perizinan'], 'integer'],
            [['nama_kolom'], 'string', 'max' => 200],
            [['keterangan'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kolom' => 'Id Kolom',
            'nama_kolom' => 'Nama Kolom',
            'keterangan' => 'Keterangan',
            'id_perizinan' => 'Jenis Perizinan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataIsianPerizinans()
    {
        return $this->hasMany(DataIsianPerizinan::className(), ['id_data_kolom' => 'id_kolom']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPerizinan()
    {
        return $this->hasOne(JenisPerizinan::className(), ['id_izin' => 'id_perizinan']);
    }
}
