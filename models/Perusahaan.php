<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "perusahaan".
 *
 * @property integer $id_perusahaan
 * @property string $nama_perusahaan
 * @property string $nama_pemilik_pj
 * @property string $kelurahan
 * @property string $jalan
 * @property string $no
 * @property string $npwp
 * @property integer $bentuk
 *
 * @property Pemohon[] $pemohons
 * @property Kelurahan $kelurahan0
 */
class Perusahaan extends \yii\db\ActiveRecord
{

    public $id_provinsi, $id_kabupaten, $id_kecamatan;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'perusahaan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_perusahaan', 'nama_pemilik_pj', 'kelurahan', 'jalan', 'no', 'npwp', 'bentuk'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['bentuk'], 'integer'],
            [['nama_perusahaan'], 'string', 'max' => 200],
            [['nama_pemilik_pj'], 'string', 'max' => 150],
            [['kelurahan'], 'string', 'max' => 10],
            [['jalan'], 'string', 'max' => 500],
            [['no'], 'string', 'max' => 5],
            [['npwp'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_perusahaan' => 'Id Perusahaan',
            'nama_perusahaan' => 'Nama Perusahaan',
            'nama_pemilik_pj' => 'Nama Pemilik Perusahaan',
            'kelurahan' => 'Kelurahan',
            'jalan' => 'Jalan',
            'no' => 'No',
            'npwp' => 'Npwp',
            'bentuk' => 'Bentuk',
            'id_provinsi' => 'Provinsi',
            'id_kabupaten' => 'Kabupaten/Kota',
            'id_kecamatan' => 'Kecamatan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPemohons()
    {
        return $this->hasMany(Pemohon::className(), ['id_perusahaan' => 'id_perusahaan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKelurahan0()
    {
        return $this->hasOne(Kelurahan::className(), ['id_kel' => 'kelurahan']);
    }
}
