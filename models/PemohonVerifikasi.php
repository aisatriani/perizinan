<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pemohon_verifikasi".
 *
 * @property integer $id_pemohon
 * @property string $verifikasi
 * @property integer $is_verifikasi
 * @property string $created_at
 * @property string $updated_at
 *
 * @property PemohonDetail $idPemohon
 */
class PemohonVerifikasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pemohon_verifikasi';
    }

    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert)){

            if($this->isNewRecord){
                $this->created_at = date('Y-m-d H:i:s');
            }{
                $this->updated_at = date('Y-m-d H:i:s');
            }

            return true;
        }
        return false;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pemohon'], 'required'],
            [['id_pemohon', 'is_verifikasi'], 'integer'],
            [['verifikasi'], 'string'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pemohon' => 'Id Pemohon',
            'verifikasi' => 'Verifikasi',
            'is_verifikasi' => 'Is Verifikasi',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPemohon()
    {
        return $this->hasOne(PemohonDetail::className(), ['pemohon' => 'id_pemohon']);
    }
}
