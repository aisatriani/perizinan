<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pemohon_detail".
 *
 * @property integer $pemohon
 * @property string $no_pemohon
 * @property integer $validasi
 * @property integer $koreksi
 * @property integer $verifikasi
 * @property integer $eksekusi
 * @property string $tgl_validasi
 * @property string $tgl_koreksi
 * @property string $tgl_verifikasi
 * @property string $tgl_eksekusi
 *
 * @property Pemohon $pemohon0
 */
class PemohonDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pemohon_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pemohon', 'no_pemohon'], 'required'],
            [['pemohon', 'validasi', 'koreksi', 'verifikasi', 'eksekusi'], 'integer'],
            [['tgl_validasi', 'tgl_koreksi', 'tgl_verifikasi', 'tgl_eksekusi'], 'safe'],
            [['no_pemohon'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pemohon' => 'Pemohon',
            'no_pemohon' => 'No Pemohon',
            'validasi' => 'Validasi',
            'koreksi' => 'Koreksi',
            'verifikasi' => 'Verifikasi',
            'eksekusi' => 'Eksekusi',
            'tgl_validasi' => 'Tgl Validasi',
            'tgl_koreksi' => 'Tgl Koreksi',
            'tgl_verifikasi' => 'Tgl Verifikasi',
            'tgl_eksekusi' => 'Tgl Eksekusi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPemohon0()
    {
        return $this->hasOne(Pemohon::className(), ['id_pemohon' => 'pemohon']);
    }

    public function getPemohonKoreksi()
    {
        return $this->hasOne(PemohonKoreksi::className(), ['id_pemohon' => 'pemohon']);
    }

    public function getPemohonVerifikasi()
    {
        return $this->hasOne(PemohonVerifikasi::className(), ['id_pemohon' => 'pemohon']);
    }

}
