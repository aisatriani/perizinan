<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "level_pengguna".
 *
 * @property integer $id_level_pengguna
 * @property string $nama_level
 * @property string $keterangan
 *
 * @property Pengguna[] $penggunas
 */
class LevelPengguna extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'level_pengguna';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_level'], 'required'],
            [['nama_level'], 'string', 'max' => 100],
            [['keterangan'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_level_pengguna' => 'Id Level Pengguna',
            'nama_level' => 'Nama Level',
            'keterangan' => 'Keterangan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengguna()
    {
        return $this->hasMany(Pengguna::className(), ['id_level_pengguna' => 'id_level_pengguna']);
    }
}
