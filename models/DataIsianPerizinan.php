<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data_isian_perizinan".
 *
 * @property integer $id_data_isian
 * @property integer $id_data_kolom
 * @property string $isian
 * @property integer $id_pemohon
 *
 * @property DataKolomPerizinan $idDataKolom
 * @property Pemohon $idPemohon
 */
class DataIsianPerizinan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_isian_perizinan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_data_kolom', 'isian', 'id_pemohon'], 'required'],
            [['id_data_kolom', 'id_pemohon'], 'integer'],
            [['isian'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_data_isian' => 'Id Data Isian',
            'id_data_kolom' => 'Id Data Kolom',
            'isian' => 'Isian',
            'id_pemohon' => 'Id Pemohon',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDataKolom()
    {
        return $this->hasOne(DataKolomPerizinan::className(), ['id_kolom' => 'id_data_kolom']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPemohon()
    {
        return $this->hasOne(Pemohon::className(), ['id_pemohon' => 'id_pemohon']);
    }
}
