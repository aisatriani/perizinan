<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "verifikasi".
 *
 * @property integer $id_verifikasi
 * @property string $nama_verifikasi
 * @property string $keterangan
 * @property integer $id_perizinan
 *
 * @property JenisPerizinan $idPerizinan
 * @property VerifikasiIsian[] $verifikasiIsians
 */
class Verifikasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'verifikasi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_verifikasi', 'id_perizinan'], 'required'],
            [['id_perizinan'], 'integer'],
            [['nama_verifikasi'], 'string', 'max' => 200],
            [['keterangan'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_verifikasi' => 'Id Verifikasi',
            'nama_verifikasi' => 'Nama Verifikasi',
            'keterangan' => 'Keterangan',
            'id_perizinan' => 'Id Perizinan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPerizinan()
    {
        return $this->hasOne(JenisPerizinan::className(), ['id_izin' => 'id_perizinan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVerifikasiIsians()
    {
        return $this->hasMany(VerifikasiIsian::className(), ['id_verifikasi' => 'id_verifikasi']);
    }
}
