<?php

namespace app\models;

use hscstudio\mimin\models\AuthAssignment;
use Yii;
use yii\base\NotSupportedException;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "pengguna".
 *
 * @property integer $id_pengguna
 * @property string $nama
 * @property string $nik
 * @property string $email
 * @property string $username
 * @property string $password
 * @property string $auth_key
 * @property string $alamat
 * @property string $telp
 * @property integer $created_at
 * @property integer $modified_at
 * @property integer $id_level_pengguna
 * @property string $id_kelurahan
 *
 * @property Pemohon[] $pemohons
 * @property Kelurahan $idKelurahan
 * @property LevelPengguna $idLevelPengguna
 */
class Pengguna extends \yii\db\ActiveRecord implements IdentityInterface
{

    const LEVEL_ADMIN = 1;

    public $password_repeat;
    public $id_provinsi;
    public $id_kabupaten;
    public $id_kecamatan;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengguna';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'nik', 'email', 'username', 'password', 'alamat', 'telp','id_provinsi','id_kabupaten','id_kecamatan','id_kelurahan'], 'required'],
            ['id_level_pengguna', 'default', 'value' => 7],
            [['password', 'alamat'], 'string'],
            [['created_at', 'modified_at', 'id_level_pengguna'], 'integer'],
            [['nama'], 'string', 'max' => 120],
            [['email', 'username'], 'string', 'max' => 60],
            [['nik'], 'string', 'min' => 16, 'message' => 'NIK harus mengandung setidaknya 16 karakter'],
            [['nik'], 'string', 'max' => 20, 'message' => 'NIK tidak lebih dari 16 karakter'],
            [['telp'], 'string', 'max' => 16],
            [['id_kelurahan'], 'string', 'max' => 10],
            [['nik'], 'unique'],
            [['email'], 'unique'],
            [['email'], 'email'],
            [['username'], 'unique'],
            //['password','compare'],
            ['password_repeat', 'compare' ,'compareAttribute' => 'password' ,'message' => 'Harap konfirmasi password anda dengan benar']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pengguna' => 'Id Pengguna',
            'nama' => 'Nama',
            'nik' => 'NIK',
            'email' => 'Email',
            'username' => 'Username',
            'password' => 'Password',
            'password_repeat' => 'Konfimasi Password',
            'alamat' => 'Alamat',
            'telp' => 'Telepon',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'id_level_pengguna' => 'Level Pengguna',
            'id_kelurahan' => 'Kelurahan',
            'id_provinsi' => 'Provinsi',
            'id_kabupaten' => 'Kabupaten',
            'id_kecamatan' => 'Kecamatan',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = Yii::$app->security->generateRandomString();
                $this->created_at = time();
            }else{
                $this->modified_at = time();
            }

            return true;
        }
        return false;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPemohon()
    {
        return $this->hasMany(Pemohon::className(), ['id_pengguna' => 'id_pengguna']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdKelurahan()
    {
        return $this->hasOne(Kelurahan::className(), ['id_kel' => 'id_kelurahan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLevelPengguna()
    {
        return $this->hasOne(LevelPengguna::className(), ['id_level_pengguna' => 'id_level_pengguna']);
    }

    public function generatePassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    public function itemsLevelPengguna()
    {
        $level = LevelPengguna::find()->all();
        return ArrayHelper::map($level, 'id_level_pengguna','nama_level');
    }

    /**
     * Finds an identity by the given ID.
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        //return Pengguna::findOne(['id_pengguna' => $id]);
        return static::findOne(['id_pengguna' => $id]);
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->id_pengguna;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function isAdmin($username)
    {
        if (static::findOne(['username' => $username, 'id_level_pengguna' => self::LEVEL_ADMIN])){

            return true;
        } else {

            return false;
        }

    }

    public function getRoles()
    {
        return $this->hasMany(AuthAssignment::className(), [
            'user_id' => 'id',
        ]);
    }
}
