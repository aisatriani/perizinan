<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pemohon_koreksi".
 *
 * @property integer $id
 * @property integer $id_pemohon
 * @property string $koreksi
 * @property integer $is_koreksi
 * @property string $created_at
 * @property string $updated_at
 *
 * @property PemohonDetail $idPemohon
 */
class PemohonKoreksi extends \yii\db\ActiveRecord
{

    const KOREKSI_YES = 1;
    const KOREKSI_NO = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pemohon_koreksi';
    }

    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert)){

            if($this->isNewRecord){
                $this->created_at = date('Y-m-d H:i:s');
            }{
                $this->updated_at = date('Y-m-d H:i:s');
            }

            return true;
        }
        return false;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pemohon', 'is_koreksi'], 'integer'],
            [['koreksi'], 'string'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_pemohon' => 'Id Pemohon',
            'koreksi' => 'Koreksi',
            'is_koreksi' => 'Is Koreksi',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPemohon()
    {
        return $this->hasOne(PemohonDetail::className(), ['pemohon' => 'id_pemohon']);
    }
}
