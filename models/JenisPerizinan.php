<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jenis_perizinan".
 *
 * @property integer $id_izin
 * @property string $nama_izin
 * @property string $singkatan
 * @property string $keterangan
 * @property integer $last_id
 * @property integer $created_at
 *
 * @property DataKolomPerizinan[] $dataKolomPerizinans
 * @property Pemohon[] $pemohons
 * @property Verifikasi[] $verifikasis
 * @property VerifikasiIsian[] $verifikasiIsians
 */
class JenisPerizinan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenis_perizinan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_izin', 'singkatan'], 'required'],
            [['last_id', 'created_at'], 'integer'],
            [['nama_izin', 'keterangan'], 'string', 'max' => 500],
            [['singkatan'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_izin' => 'Id Izin',
            'nama_izin' => 'Nama Perizinan',
            'singkatan' => 'Singkatan',
            'keterangan' => 'Keterangan',
            'last_id' => 'Last ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataKolomPerizinan()
    {
        return $this->hasMany(DataKolomPerizinan::className(), ['id_perizinan' => 'id_izin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPemohon()
    {
        return $this->hasMany(Pemohon::className(), ['id_jenis_perizinan' => 'id_izin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVerifikasi()
    {
        return $this->hasMany(Verifikasi::className(), ['id_perizinan' => 'id_izin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVerifikasiIsian()
    {
        return $this->hasMany(VerifikasiIsian::className(), ['id_jenis_perizinan' => 'id_izin']);
    }
}
